"""All urls contained in this file will be appended to the er2_reset URL namespace.

For instance, if a url pattern of the form:

django.conf.urls.url(r'$', views.index)

Is included as a url pattern, the URL "<app_name>/" will be routed to er2_reset.views.index
"""
import django.conf.urls

from er2.apps.er2_reset import views

urlpatterns = [
    django.conf.urls.url(r'api/v1/state/$', views.state),
    django.conf.urls.url(r'imagery/$', views.find_imagery),
    django.conf.urls.url(r'imagery_row_path/$', views.find_imagery_row_path),
    django.conf.urls.url(r'scene/$', views.set_scene),
    django.conf.urls.url(r'api/v1/climate/$', views.climate),
    django.conf.urls.url(r'api/v1/generate_inputs/', views.generate_inputs),
    django.conf.urls.url(r'api/v1/process/', views.process_scene),
    django.conf.urls.url(r'download_climate/', views.download_climate),
]
