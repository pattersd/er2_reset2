"""Provides optional install hooks for the application.

When an app is installed using the er2 installation script, you may provide logic in the
functions below to more easily build your app.
"""
import distutils.dir_util
import glob
import logging
import subprocess
import os
import shutil
import sys
import tempfile


HERE = os.path.abspath(os.path.dirname(os.path.abspath(__file__)))
# Mimic the django PYTHONPATH to use the same imports when running as a script
if __name__ == "__main__":
    sys.path.append(os.path.join(HERE, os.pardir, os.pardir, os.pardir))
import er2.deploy.start as er2_start


STATIC = os.path.join(HERE, "static")


def post_install(**kwargs):
    """Runs after the repository has been deployed to its destination location"""
    install_npm_dependencies(**kwargs)


def deploy(**kwargs):
    """Runs deployment tasks relevant for the app"""
    build_code(**kwargs)


def package(**kwargs):
    """Runs packaging tasks relevant for the app"""
    build_dir = os.path.join(HERE, "deploy", "__build__")
    if os.path.exists(build_dir):
        logging.getLogger("er2").info("Removing old er2_reset build: %s", build_dir)
        shutil.rmtree(build_dir)
    os.makedirs(build_dir)
    copy_source_code(build_dir)
    copy_build_files(build_dir)
    package_code(build_dir)
    remove_built_code(build_dir)


def remove_built_code(build_dir):
    """Removes the app directory.

    This is necessary for Jenkins builds, where docker jobs mess up file permissions for subsequent builds.
    """
    logging.getLogger("er2").info("Removing build directory")
    shutil.rmtree(build_dir)


def copy_source_code(build_dir):
    """Copies source code used by the app into a location that can be injected into the bundled app.

    Arguments:
        build_dir (str): the root of the build directory. Secrets will be placed within "build_dir/src"
    """
    src_dir = os.path.abspath(os.path.join(HERE, os.pardir, os.pardir, os.pardir))
    tmp_dir = tempfile.mkdtemp()
    dest_dir = os.path.join(build_dir, "src")
    if os.path.exists(dest_dir):
        shutil.rmtree(dest_dir)
    logging.getLogger("er2").info(
        "Copying source code for er2_reset from {0} to {1}".format(src_dir, tmp_dir)
    )
    shutil.copytree(
        src_dir,
        os.path.join(tmp_dir, "app"),
        ignore=shutil.ignore_patterns("node_modules", ".hg", ".git"),
    )
    shutil.move(tmp_dir, dest_dir)


def copy_build_files(build_dir):
    """Copies docker build files used by the app into a location that can be used during the docker build step.

    Arguments:
        build_dir (str): the root of the build directory. Secrets will be placed within "build_dir"
    """
    src_dir = os.path.abspath(os.path.join(HERE, "deploy", "publish"))
    dest_dir = os.path.join(build_dir)
    if not os.path.exists(dest_dir):
        os.makedirs(dest_dir)
    try:
        logging.getLogger("er2").info("Copying build files for er2_reset")
        cmd = "cp -r %s/* %s" % (src_dir, dest_dir)
        er2_start.get_output(cmd)
    except subprocess.CalledProcessError as exc:
        logging.getLogger("er2").warn("Error copying build files:\n%s", exc.output)
        logging.getLogger("er2").warn("Please run the following manually: \n%s", cmd)


def package_code(build_dir):
    try:
        logging.getLogger("er2").info("Packaging er2_reset")
        image_org = os.getenv("image_org", "pattersd")
        image_name = os.getenv("image_name", "reset")
        image_tag = os.getenv("image_tag", "latest")
        docker_user = os.getenv("docker_user", "notauser")
        docker_pw = os.getenv("docker_pw", "notasecret")
        cmd = "docker build -t %s/%s:%s %s" % (
            image_org,
            image_name,
            image_tag,
            build_dir,
        )
        logging.getLogger("er2").info(cmd)
        er2_start.get_output(cmd)
        cmd = "docker login --username=%s --password=%s" % (docker_user, docker_pw)
        er2_start.get_output(cmd)
        cmd = "docker push %s/%s:%s" % (image_org, image_name, image_tag)
        logging.getLogger("er2").info(cmd)
        er2_start.get_output(cmd)
        # remove_built_code()
    except subprocess.CalledProcessError as exc:
        logging.getLogger("er2").warn("Error packaging code:\n%s", exc.output)
        logging.getLogger("er2").warn(
            "Please run the following manually (from %s): \n%s", STATIC, cmd
        )


def build_code(**kwargs):
    for app in ["er2_map", "er2_map"]:
        app_dir = os.path.join(HERE, "../{0}/static").format(app)
        try:
            args = ("-it --rm --user $(id -u)", os.path.join(app_dir, "static"))
            if "tty_enabled" in kwargs and kwargs["tty_enabled"] is False:
                args = ("--rm --user $(id -u)", os.path.join(app_dir, "static"))
            logging.getLogger("er2").info("Building er2_reset")
            cmd = (
                "docker run %s -v %s:/static/ erams/er2_web_build /bin/bash "
                '-c "cd /static && npm run build"' % args
            )
            er2_start.get_output(cmd)
        except subprocess.CalledProcessError as exc:
            logging.getLogger("er2").warn("Error building code:\n%s", exc.output)
            logging.getLogger("er2").warn(
                "Please run the following manually (from %s): \n%s", STATIC, cmd
            )


def install_npm_dependencies(**kwargs):
    """Installs npm dependencies via Yarn"""
    try:
        logging.getLogger("er2").info("Installing npm modules")
        args = ("-it --rm --user $(id -u)", os.path.join(HERE, "static"))
        if "tty_enabled" in kwargs and kwargs["tty_enabled"] is False:
            args = ("--rm --user $(id -u)", os.path.join(HERE, "static"))
        cmd = (
            "docker run %s -v %s:/static/ erams/er2_web_build /bin/bash "
            '-c "cd /static && yarn"' % args
        )
        er2_start.get_output(cmd)
    except subprocess.CalledProcessError as exc:
        logging.getLogger("er2").warn("Error installing npm modules:\n%s", exc.output)
        logging.getLogger("er2").warn("Please run the following manually: \n%s", cmd)


if __name__ == "__main__":
    logger = logging.getLogger("er2")
    logger.setLevel(logging.INFO)
    console_handler = logging.StreamHandler()
    # formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s")
    # console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)
    package()
