"""
WSGI config for er2 project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os, sys

from django.core.handlers.wsgi import WSGIHandler
import django

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../../")))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../")))

class WSGIEnvironment(WSGIHandler):
    def __call__(self, environ, start_response):
        return super(WSGIEnvironment, self).__call__(environ, start_response)


os.environ.setdefault("ER2_GEOPROCESSING_DB_HOST", "localhost")
os.environ.setdefault("REDIS_HOST", "localhost")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "er2.settings")
django.setup(set_prefix=False)
application = WSGIEnvironment()
