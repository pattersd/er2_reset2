#!/usr/bin/env bash
set -e

echo "export CSIP_HOST=${CSIP_HOST}" >> /etc/apache2/envvars
echo "export CACHE_DB_HOST=${CACHE_DB_HOST}" >> /etc/apache2/envvars
echo "export CACHE_DB_PORT=${CACHE_DB_PORT}" >> /etc/apache2/envvars
echo "export EMAIL_PASSWORD=${EMAIL_PASSWORD}" >> /etc/apache2/envvars
echo "export MONGODB_HOST=${MONGODB_HOST}" >> /etc/apache2/envvars
echo "export STAC_API_URL=${STAC_API_URL}" >> /etc/apache2/envvars

umask 0000
ln -s /tmp /var/www/html/tmp

# Run cache_db cron job
chmod 600 /var/spool/cron/crontabs/root
cron &

# For FCGID version we need to copy mapserv to mapservfcgi
cp /usr/lib/cgi-bin/mapserv /usr/lib/cgi-bin/mapserv.fcgi

nohup redis-server &
service apache2 start
echo "You may now connect to http://localhost to run ReSET"
tail -f /dev/null
