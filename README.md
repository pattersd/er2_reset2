# {{ app_name }} #

{{ app_name }} is Catena app built with [Django](https://www.djangoproject.com/).

### What is this repository for? ###

{{ app_name }} serves as a prototype for new eRAMS 2.0 applications, containing examples for how to build your
own geospatial application.

### How do I get set up? ###

To install, download the [er2_web](https://bitbucket.org/erams2/er2_web) repository and
run `python /path/to/er2/app/er2/deploy/install.py https://bitbucket.org/erams2/{{ app_name }} [-v version]`.

Once the app is installed, run the app with `python /path/to/er2/app/er2/deploy/start.py`

You will need to build the application's static assets before being able to run the application.  You can do this by running the docker command:

```bash
docker run -it --rm --net er2_default -v /path/to/{{ app_name }}/app/er2/apps/{{ app_name }}/static/:/static erams/er2_web_build /bin/bash -c "cd /static && npm run dev"
```

Subsequent webpack builds can be done automatically by using the --watch flag in the webpack command.


### Contribution guidelines ###

Feel free to submit a pull request for patches, bug reports, or feature requests.  If committing code,
please peruse the [style guidelines](https://bitbucket.org/erams2/er2_style_guides) before submitting
a pull request.

### Who do I talk to? ###

* Kyle Traff ([email](mailto:kyle.traff@colostate.edu))
* David Patterson ([email](mailto:david.patterson@colostate.edu))
* Olaf David ([email](mailto:olaf.david@colostate.edu))
