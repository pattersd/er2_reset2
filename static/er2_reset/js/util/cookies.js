import Cookies from 'universal-cookie'

export function getCookie(name) {
    const cookies = new Cookies()
    return cookies.get(name)
}

export function setCookie(name, value) {
    const cookies = new Cookies()
    cookies.set(name, value, { path: '/' })
}

export function removeCookie(name) {
    const cookies = new Cookies()
    cookies.remove(name)
}

export const getCookieVal = (cookie, defval) => {
    const cval = getCookie(cookie)
    return cval !== undefined ? cval : defval
}
