// @flow
import { parse } from 'query-string'

export function parseLocation(location: { pathname: string, search?: string }): { path: string, params: {} } {
    return { path: location.pathname, params: parse(location.search) || {} }
}

/**
 * Return the mapserver host location
 * @returns {string}
 */
export const getMapserverHost = () => {
    let server = window.location.origin
    // Check for django dev server, in which case mapserver requests just go to port 80
    if (window.location.port >= 8000) {
        server = `${window.location.protocol}//${window.location.hostname}`
    }
    return server
}
