// @flow
import Geojson from 'ol/format/GeoJSON'

const PROJ_3857 = 'EPSG:3857'
const PROJ_4326 = 'EPSG:4326'
export function writeFeatures(feats, opts = {}) {
    const geojson = new Geojson()
    return geojson.writeFeatures(feats, {
        featureProjection: PROJ_3857,
        dataProjection: PROJ_4326,
    })
}

export function readFeatures(feats, opts = {}) {
    const geojson = new Geojson()
    const featureProjection = opts.featureProjection || PROJ_3857
    const dataProjection = opts.dataProjection || PROJ_4326

    return geojson.readFeatures(feats, {
        featureProjection: PROJ_3857,
        dataProjection: PROJ_4326,
    })
}

