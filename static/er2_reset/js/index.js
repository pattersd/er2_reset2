// @flow
import React from "react"
import { render } from "react-dom"
import { Provider } from "react-redux"
import { BrowserRouter } from "react-router-dom"
import { applyMiddleware, compose, createStore } from "redux"
import thunkMiddleware from "redux-thunk"
import ReactGA from "react-ga"
import App from "./containers/app"
import reducer from "./reducers"
import "../css/global.css"

const store = createStore(
    reducer,
    compose(
        applyMiddleware(thunkMiddleware),
        window.__REDUX_DEVTOOLS_EXTENSION__
            ? window.__REDUX_DEVTOOLS_EXTENSION__()
            : (f) => f,
    ),
)

ReactGA.initialize("UA-192217785-1")
ReactGA.event({
    category: "Initial Load",
    action: "load",
    label: "Initial",
})

render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById("root"),
)
