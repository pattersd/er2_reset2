// Combines all reducers for the application into a single exportable object
import { combineReducers } from "redux";
import {
  geoBarReducer as geoBar,
  userReducer as user,
} from "@owsi/catena/er2_ui";
import {
  attributeTables,
  identify,
  logger,
  map,
  mapBaseLayer,
  mapsources,
  publicSites,
  tabbedPanel,
  theme,
} from "@owsi/catena/er2_map_userlayers/js/reducers";
import services from "@owsi/catena/er2_map_services/js/reducers/services";
import climate from "./climate";
import contextBar from "./context_bar";
import tableOfContents from "./table_of_contents";
import mapToolbar from "./map_toolbar";
import meta from "./meta";
import processOptions from "./process_options";
import processScene from "./process_scene";
import selectAOI from "./selectAOI";
import selectClimate from "./select_climate";
import selectImage from "./select_image";
import serverState from "./server_state";
import settings from "./settings";
import token from "./token";

const rootReducer = combineReducers({
  attributeTables,
  climate,
  contextBar,
  geoBar,
  identify,
  logger,
  map,
  mapBaseLayer,
  mapsources,
  mapToolbar,
  meta,
  processOptions,
  processScene,
  publicSites,
  selectAOI,
  selectClimate,
  selectImage,
  serverState,
  services,
  settings,
  tabbedPanel,
  tableOfContents,
  theme,
  token,
  user,
});

export default rootReducer;
