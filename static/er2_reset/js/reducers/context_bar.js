// @flow
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalculator, faCog, faHome, faImage, faMapPin, faThermometerHalf } from '@fortawesome/free-solid-svg-icons'
import { CLICK_CONTEXT_BAR_ITEM } from '../constants/action_types'
import * as ids from '../constants/ids'
import * as routes from '../constants/routes'

/**
 * Returns true if the url matches the item's link.
 *
 * If the item doesn't have a link, check's the item's active status.
 */
function isActive(location: { path: string }, item: { active: boolean }, exactMatch: boolean = false) {
    if (item.link) {
        return exactMatch
            ? location.path === item.link
            : location.path.startsWith(item.link)
    }
    return item.active
}

const initialState = {
    items: [{
        icon: { default: <FontAwesomeIcon icon={faHome} /> },
        id: ids.DOCUMENTATION_LINK,
        isActive: location => location.path === routes.HOME
            || location.path.startsWith(routes.INSTRUCTIONS),
        label: 'Documentation',
        name: 'Documentation',
        link: routes.HOME,
    }, {
        icon: { default: <img src={'/static/er2_reset/img/agroet_icon.png'} /> },
        id: ids.SELECT_IMAGE,
        isActive,
        label: 'Select LANDSAT Image',
        name: 'Select LANDSAT Image',
        link: routes.SELECT_IMAGE,
    }, {
        icon: { default: <FontAwesomeIcon icon={'cogs'} /> },
        id: ids.PROCESS_OPTIONS,
        isActive,
        label: 'Select image processing pptions',
        name: 'Process Options',
        link: routes.PROCESS_OPTIONS,
    }, {
        label: 'Process Scene',
        icon: { default: <FontAwesomeIcon icon={'satellite'} /> },
        id: ids.PROCESS_SCENE,
        isActive,
        name: 'Process Scene',
        link: routes.PROCESS_SCENE,
    }, {
        icon: { default: <FontAwesomeIcon icon={faCog} /> },
        id: 'settings',
        isActive,
        label: 'Settings',
        name: 'Settings',
        link: routes.SETTINGS,
        position: 'bottom',
    }],
}

export default function contextBar(state = initialState, action) {
    switch (action.type) {
    case CLICK_CONTEXT_BAR_ITEM:
        return {
            ...state,
            items: state.items.map(item => (
                item.id === action.id ?
                    { ...item, active: true } :
                    { ...item, active: false }
            )),
        }

    default:
        return state
    }
}
