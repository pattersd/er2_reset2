// @flow
import React from 'react'
import * as types from '../constants/action_types'
import * as ids from '../constants/ids'

export type ProcessOptionsType = {
    visible: boolean,
    options: {
        noFillGaps: boolean,
        uncalibratedMode: boolean,
    },
}

const initialState = {
    visible: true,
    options: {
        LS7_FILL_GAP: {
            label: 'Fill gaps when processing LANDSAT 7 images',
            value: true,
            help: (<span>LANDSAT 7&apos;s sensor is broken.</span>),
        },
        USER_UNCALIB: {
            label: 'Use uncalibrated mode',
            value: false,
            help: (<em>TODO.</em>),
        },
    },
}

export default function processOptions(state: ProcessOptionsType = initialState, action) {
    let newstate = state
    if (action.type === types.SET_FETCHED_STATE) {
        const appState = action.state.appState
        if (appState && appState.processScene) {
            newstate.options = { ...initialState.options }
        }
    } else if (action.type === types.CLICK_CONTEXT_BAR_ITEM) {
        if (action.id === ids.PROCESS_OPTIONS) {
            // Toggle the control bar if clicking an already-active select image button.
            newstate = (action.active
                ? { ...state, visible: !state.visible }
                : { ...state, visible: true })
        }
    } else if (action.type === types.SET_RUN_OPTION) {
        newstate = { ...state, options: { ...state.options } }
        newstate.options[action.option].value = action.checked
    }
    return newstate
}
