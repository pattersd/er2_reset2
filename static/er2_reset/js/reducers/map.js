// @flow
import type { ExtentType } from "@owsi/catena/er2_map/src/map";
import type { UserMapsourceType } from "@owsi/catena/er2_map_userlayers/js/reducers/mapsources";
import * as actionTypes from "../constants/action_types";

export type ViewType = {
  center: Array<number>,
  resolution: number,
  rotation: number,
  extent: ExtentType,
  // Extend er2_map's ViewType to store the mapScale.
  mapScale: number,
};

export type MapType = {
  baseLayer: string,
  // Last requested extent. The map will only process if it changes.
  extent?: ExtentType,
  mapView: ViewType,
  // TODO: expand this to include the other source types
  mapsources: Array<UserMapsourceType>,
  userLayersMapfile?: string,
};

type MapActionType = {
  type: string,
  id: string,
  baseLayer?: string,
  extent?: ExtentType,
  ficusData?: FicusDataType,
  mapScale?: number,
  mapView?: ViewType,
  mapsources?: Array<UserMapsourceType>,
};

const initialState = {
  baseLayer: "osm",
  mapsources: [],
  mapView: {
    center: [0, 0],
    resolution: 0,
    rotation: 0,
    mapScale: 0,
  },
};

export default function map(
  state: MapType = initialState,
  action: MapActionType
) {
  let newstate = state;
  if (action.type === actionTypes.UPDATE_MAP_VIEW) {
    newstate = {
      ...state,
      mapView: action.mapView,
    };
  } else if (
    action.type === actionTypes.SET_FETCHED_STATE &&
    action.state.mapsources
  ) {
    newstate = {
      ...state,
      mapsources: action.state.mapsources,
    };
  }

  return newstate;
}
