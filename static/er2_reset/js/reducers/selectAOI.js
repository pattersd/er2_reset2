// @flow
import * as types from '../constants/action_types'
import * as ids from '../constants/ids'

export type SelectAOItype = {
    aoi: string,
    isRunning: boolean,
    visible: boolean,
}

type ActionType = {
    type: string,
    aoi: {
        features: [],
    },
}

const initialState = {
    aoi: {
        features: [],
        type: 'FeatureCollection',
    },
    isRunning: false,
    visible: true,
}

export default function selectAOI(state: SelectAOItype = initialState, action: ActionType) {
    let ret = state
    if (action.type === types.SET_FETCHED_STATE) {
        const appState = action.state.appState
        if (appState && appState.selectAOI) {
            ret = { ...appState.selectAOI, isRunning: false }
            // Changed AOI from WKT to geojson
            if (ret.aoi && typeof ret.aoi === 'string') {
                ret.aoi = { features: [] }
            }
        }
    } else if (action.type === types.CLICK_CONTEXT_BAR_ITEM) {
        if (action.id === ids.SELECT_AOI) {
            // Toggle the control bar if clicking an already-active select image button.
            ret = (action.active
                ? { ...state, visible: !state.visible }
                : { ...state, visible: true })
        }
    } else if (action.type === types.AOI_RUNNING) {
        ret = { ...state, isRunning: action.isRunning }
    } else if (action.type === types.SET_AOI) {
        ret = { ...state, aoi: action.aoi }
    }
    return ret
}
