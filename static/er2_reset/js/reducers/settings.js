// @flow
import * as actionTypes from '../constants/action_types'
import * as ids from '../constants/ids'

export type SettingsBarType = {
    visible: boolean,
    selectionType: 'ee' | 'satsearch',
}

const initialState = {
    visible: true,
    selectionType: 'ee',
}

export default function SettingsBar(state: SettingsBarType = initialState, action) {
    let newState = state

    if (action.type === actionTypes.CLICK_CONTEXT_BAR_ITEM) {
        if (action.id === ids.SETTINGS) {
            // Toggle the control bar if clicking an already-active framework button.
            // Otherwise, always show the control bar.
            newState = (action.active
                ? { ...state, visible: !state.visible }
                : { ...state, visible: true })
        }
    } else if (action.type === actionTypes.UPDATE_SETTINGS) {
        newState = {
            ...state,
            ...action.settings,
        }
    }

    return newState
}
