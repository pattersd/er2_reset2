// @flow
import * as types from '../constants/action_types'

export type GeoJSONType = {
    geometry: string,
    id: number,
    properties: {},
}

export type ClimateType = {
    stations: GeoJSONType[],
}

type ActionType = {
    type: string,
    checked?: boolean,
    climate?: GeoJSONType[],
    id?: number,
}

const initialState = {
    stations: [],
}

export default function climate(state: ClimateType = initialState, action: ActionType) {
    let ret = state
    if (action.type === types.SET_FETCHED_STATE) {
        const appState = action.state.appState
        if (appState && appState.climate) {
            ret = action.state.appState.climate
        }
    } else if (action.type === types.RESET_SEARCH) {
        ret = { ...state, stations: [] }
    } else if (action.type === types.SET_AOI) {
        ret = { ...state, stations: [] }
    } else if (action.type === types.SET_CURRENT_SCENE) {
        // Changing the scene will unset climate
        ret = { ...state, stations: [] }
    } else if (action.type === types.SET_STATION_CHECKED) {
        const station = state.stations.find(s => s.id === action.id)
        station.properties.ignore = !action.checked
        ret = { ...state, stations: [...state.stations] }
    } else if (action.type === types.UPDATE_CLIMATE_STATIONS) {
        ret = { ...state, stations: action.climate }
    }
    return ret
}
