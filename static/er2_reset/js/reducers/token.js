// @flow
// Manages the session's application token
import * as actionTypes from "../constants/action_types"

export type TokenType = {
    value: string,
    expires: number,
}

const initialState = {}

export default function token(state: TokenType = initialState, action) {
    switch (action.type) {
        case actionTypes.SET_FETCHED_STATE:
            return {
                ...state,
                ...action.state.token,
                value: "auth",
            }

        default:
            return state
    }
}
