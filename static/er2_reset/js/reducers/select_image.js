// @flow
import * as ids from "../constants/ids"
import * as types from "../constants/action_types"
import type { GeoJSONType } from "./climate"

export type ImageryType = {
    cloud: number,
    date: string,
    path: string,
    row: string,
    sat_type: string,
    id: string,
    thumbnail: string,
}

export type SelectImageType = {
    endDate: string,
    imageryResults: ImageryType[],
    isRunning: boolean,
    path: string,
    row: string,
    scene: ImageryType,
    searchFeature: GeoJSONType,
    startDate: string,
    visible: boolean,
}

type ActionType = {
    type: string,
    date: string,
    dateType: string,
    isRunning?: boolean,
    results?: ImageryType[],
    scene?: ImageryType,
    searchFeature?: GeoJSONType,
}

const initialState = {
    endDate: undefined, // new Date(),
    imageryResults: [],
    isRunning: false,
    path: "",
    row: "",
    searchFeature: null,
    scene: null,
    startDate: undefined, // subMonths(new Date(), 1),
    visible: true,
}

export default function selectImage(
    state: SelectImageType = initialState,
    action: ActionType,
) {
    let ret = state
    if (action.type === types.SET_FETCHED_STATE) {
        const appState = action.state.appState
        if (appState && appState.selectImage) {
            ret = { ...appState.selectImage, isRunning: false }
        }
    } else if (action.type === types.CLICK_CONTEXT_BAR_ITEM) {
        if (action.id === ids.SELECT_IMAGE) {
            // Toggle the control bar if clicking an already-active select image button.
            ret = action.active
                ? { ...state, visible: !state.visible }
                : { ...state, visible: true }
        }
    } else if (action.type === types.RESET_SEARCH) {
        ret = { ...state, imageryResults: [], scene: null }
    } else if (action.type === types.UPDATE_IMAGERY) {
        ret = { ...state, imageryResults: action.results, isRunning: false }
    } else if (action.type === types.SET_SEARCH_FEATURE) {
        ret = { ...state, searchFeature: action.searchFeature }
    } else if (action.type === types.SET_SEARCH_DATE) {
        ret = { ...state, [action.dateType]: action.date }
    } else if (action.type === types.SET_CURRENT_SCENE) {
        ret = { ...state, scene: action.scene }
    } else if (action.type === types.UPDATE_SCENE_RESULTS) {
        ret = { ...state, sceneResults: action.results, isRunning: false }
    } else if (action.type === types.SEARCH_RUNNING) {
        ret = { ...state, isRunning: action.isRunning, scene: null }
    } else if (action.type === types.SET_PATH) {
        ret = { ...state, path: action.path }
    } else if (action.type === types.SET_ROW) {
        ret = { ...state, row: action.row }
    }
    return ret
}
