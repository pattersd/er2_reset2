// @flow
import { mapToolbarReducer, mapToolbarInitialState } from "@owsi/catena/er2_ui";
import * as mapActionTypes from "@owsi/catena/er2_map_userlayers/js/constants/action_types";

// Override the behavior of the geo bar, or disable it here
const initialState = { ...mapToolbarInitialState };
export default function mapToolbarChild(state = initialState, action) {
  let newState = state;
  // Listen for getAttributeTable, in which case we set the maptoolbar to be closed
  if (action.type === mapActionTypes.ATTRIBUTE_TABLE_SHOW) {
    newState = {
      ...state,
      panel: {
        visible: false,
      },
    };
  }

  return mapToolbarReducer(newState, action);
}
