// @flow
import * as types from '../constants/action_types'
import type { GeoJSONType } from './climate'
import * as ids from '../constants/ids'

export type SelectClimateType = {
    cold_aoi: GeoJSONType,
    cold_pts: GeoJSONType,
    hasHotAndColdAOIs: boolean,
    hasHotAndColdPoints: boolean,
    hot_aoi: GeoJSONType,
    hot_pts: GeoJSONType,
    visible: boolean,
}

type ActionType = {
    type: string,
    cold_aoi: [],
    cold_pts: [],
    features: [],
    hot_aoi: [],
    hot_pts: [],
    hasHotAndColdAOIs?: boolean,
    hasHotAndColdPoints?: boolean,
    pointType?: 'cold' | 'hot',
    shapeType?: 'pts' | 'aoi',
}

const initialState = {
    cold_aoi: { features: [] },
    cold_pts: { features: [] },
    hot_aoi: { features: [] },
    hot_pts: { features: [] },
    hasHotAndColdAOIs: false,
    hasHotAndColdPoints: false,
    visible: true,
}

const updateClimate = (state, climate) => ({
    ...state,
    cold_aoi: climate.cold.aoi,
    cold_pts: climate.cold.pts,
    hot_aoi: climate.hot.aoi,
    hot_pts: climate.hot.pts,
})

export default function selectClimate(state: SelectClimateType = initialState, action: ActionType) {
    let ret = state
    if (action.type === types.SET_FETCHED_STATE) {
        const appState = action.state.appState
        if (appState && appState.selectClimate) {
            ret = action.state.appState.selectClimate
        }
    } else if (action.type === types.CLICK_CONTEXT_BAR_ITEM) {
        if (action.id === ids.SELECT_CLIMATE) {
            // Toggle the control bar if clicking an already-active select image button.
            ret = (action.active
                ? { ...state, visible: !state.visible }
                : { ...state, visible: true })
        }
    } else if (action.type === types.HAS_HOT_AND_COLD_POINTS) {
        ret = { ...state, hasHotAndColdAOIs: false, hasHotAndColdPoints: action.hasHotAndColdPoints }
    } else if (action.type === types.HAS_HOT_AND_COLD_AOIS) {
        ret = { ...state, hasHotAndColdPoints: false, hasHotAndColdAOIs: action.hasHotAndColdAOIs }
    } else if (action.type === types.SET_POINTS) {
        const layerName = `${action.pointType}_${action.shapeType}`
        ret = { ...state, [layerName]: action.features }
    } else if (action.type === types.UPDATE_RESET_CLIMATE) {
        ret = updateClimate(state, action.resetClimate)
    }
    return ret
}
