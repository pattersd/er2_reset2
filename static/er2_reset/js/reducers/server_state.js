// @flow
// Manages application state fetched from the server
import * as actionTypes from '../constants/action_types'

export type ServerStateType = {
    isFetched: boolean,
    isFetching: boolean,
    exception: {
        message: string,
        traceback: string,
    },
}

const initialState = {
    isFetched: false,
    isFetching: false,
}

export default function serverState(state: ServerStateType = initialState, action) {
    switch (action.type) {
    case actionTypes.SET_FETCHING_STATE:
        return {
            ...state,
            isFetching: action.isFetching,
        }
    case actionTypes.SET_FETCHED_STATE:
        return {
            ...state,
            isFetched: action.isFetched,
        }

    default:
        return state
    }
}
