// @flow
import React, { useEffect } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { connect } from "react-redux"
import { NavLink, Redirect, Route, withRouter } from "react-router-dom"
import { bindActionCreators } from "redux"
import { library } from "@fortawesome/fontawesome-svg-core"
import {
    faBell,
    faBullseye,
    faCaretDown,
    faCaretUp,
    faChartPie,
    faCogs,
    faCopy,
    faDatabase,
    faDrawPolygon,
    faEdit,
    faEyeSlash,
    faHome,
    faInfoCircle,
    faItalic,
    faInfo,
    faMinus,
    faPlay,
    faSatellite,
    faSlidersH,
    faSync,
    faUpload,
    faWindowClose,
} from "@fortawesome/free-solid-svg-icons"
import { parse, stringify } from "query-string"
import { makeStyles } from "@material-ui/styles"
import { MuiThemeProvider } from "@material-ui/core/styles"
import Header from "@owsi/catena/er2_ui/components/header"
import Breadcrumbs from "@owsi/catena/er2_ui/components/breadcrumbs"
import ContextBar from "@owsi/catena/er2_ui/components/context_bar"
import ErrorBoundary from "@owsi/catena/er2_map_userlayers/js/components/error_boundary"
import AttributeTable from "@owsi/catena/er2_map_userlayers/js/components/attribute_table/attribute_table_stable"
import User from "@owsi/catena/er2_ui/components/user"
import UserLayersContainer from "@owsi/catena/er2_map_userlayers/js/containers/userlayers"
import { UserSettings } from "@owsi/catena/er2_ui"
import { customMuiTheme } from "@owsi/catena/er2_styles/mui"
import TabbedPanel from "@owsi/catena/er2_map_userlayers/js/components/tabbed_panel"
import WindowState, {
    windowStates,
} from "@owsi/catena/er2_map_userlayers/js/components/window_state"
import type {
    ActionsType,
    HistoryType,
    LocationType,
} from "@owsi/catena/er2_map_userlayers/js/reducers"
import type { AttributeTablesType } from "@owsi/catena/er2_map_userlayers/js/reducers/attribute_tables"
import type { IdentifyType } from "@owsi/catena/er2_map_userlayers/js/reducers/identify"
import type { LoggerType } from "@owsi/catena/er2_map_userlayers/js/reducers/logger"
import type { MapType } from "@owsi/catena/er2_map_userlayers/js/reducers/map"
import type { MapBaseLayerType } from "@owsi/catena/er2_map_userlayers/js/reducers/mapbaselayer"
import type { MapsourcesType } from "@owsi/catena/er2_map_userlayers/js/reducers/mapsources"
import type { ServicesType } from "@owsi/catena/er2_map_services/js/reducers/services"
import type { SitesType } from "@owsi/catena/er2_map_userlayers/js/reducers/public_sites"
import type { TabbedPanelType } from "@owsi/catena/er2_map_userlayers/js/reducers/tabbed_panel"
import type { ThemeType } from "@owsi/catena/er2_map_userlayers/js/reducers/theme"
import type { UserType } from "@owsi/catena/er2_ui/reducers/user"
import * as colors from "../styles/colors"
import styles from "../styles"
import Climate from "../components/climate"
import ProcessOptions from "../components/process_options"
import ProcessScene from "../components/process_scene"
import ResetMap from "../components/reset_map"
import SelectClimate from "../components/select_climate"
import SelectImage from "../components/select_image"
import Settings from "../components/settings"
import * as Actions from "../actions"
import Instructions from "../components/instructions"
import TableOfContents from "../components/table_of_contents"
import * as ids from "../constants/ids"
import * as routes from "../constants/routes"
import type { ClimateType } from "../reducers/climate"
import type { ProcessOptionsType } from "../reducers/process_options"
import type { ProcessSceneType } from "../reducers/process_scene"
import type { SelectAOItype } from "../reducers/selectAOI"
import type { SelectClimateType } from "../reducers/select_climate"
import type { SelectImageType } from "../reducers/select_image"
import type { ServerStateType } from "../reducers/server_state"
import type { SettingsBarType } from "../reducers/settings"
import type { TableOfContentsType } from "../reducers/table_of_contents"
import type { TokenType } from "../reducers/token"

library.add(
    faBell,
    faBullseye,
    faCaretDown,
    faCaretUp,
    faChartPie,
    faCogs,
    faCopy,
    faDatabase,
    faDrawPolygon,
    faEdit,
    faEyeSlash,
    faHome,
    faItalic,
    faInfo,
    faInfoCircle,
    faMinus,
    faPlay,
    faSatellite,
    faSlidersH,
    faSync,
    faUpload,
    faWindowClose,
)

type PropsType = {
    actions: {
        ...ActionsType,
        generateInputs: Function,
        onSetScene: Function,
        onFindImagery: Function,
        onFindImageryByRowAndPath: Function,
        onLogout: Function,
        onProcessScene: Function,
        onUserLayerDownload: Function,
        onUserLayersPropsChange: Function,
        onSetStationChecked: Function,
        setAOI: Function,
        setHasHotAndColdAOIs: Function,
        setHasHotAndColdPoints: Function,
        setPoints: Function,
        setSceneDate: Function,
        setSearchDate: Function,
        setSearchFeature: Function,
        setSearchPath: Function,
        setSearchRow: Function,
        updateSettings: Function,
        uploadAOI: Function,
        uploadResetClimate: Function,
    },
    attributeTables: AttributeTablesType,
    classes: styles,
    climate: ClimateType,
    contextBar: {},
    geoBar: any,
    history: HistoryType,
    identify: IdentifyType,
    location: LocationType,
    logger: LoggerType,
    map: MapType,
    mapBaseLayer: MapBaseLayerType,
    mapsources: MapsourcesType,
    mapToolbar: any,
    processOptions: ProcessOptionsType,
    processScene: ProcessSceneType,
    publicSites: SitesType,
    selectAOI: SelectAOItype,
    selectClimate: SelectClimateType,
    selectImage: SelectImageType,
    serverState: ServerStateType,
    services: ServicesType,
    settings: SettingsBarType,
    tabbedPanel: TabbedPanelType,
    tableOfContents: TableOfContentsType,
    theme: ThemeType,
    token: TokenType,
    user: UserType,
}

const useStyles = makeStyles(styles)

function App(propsUnstyled: PropsType) {
    // Apply theme to default styles
    const classes = useStyles({ ...propsUnstyled, ...colors })
    const theprops = { ...propsUnstyled, classes }
    const catenaMap = React.useRef(null)

    useEffect(() => {
        // Fetch the application state once, on initial load
        theprops.actions.fetchState()

        // Check if the route doesn't match anything
        const pathnames = Object.entries(routes).map((entry) => entry[1])
        if (!pathnames.includes(theprops.location.pathname)) {
            const params = parse(theprops.location.search)
            theprops.history.push({
                pathname: routes.HOME,
                search: stringify(params),
            })
        }
    }, [])

    const logout = () => {
        theprops.actions.onLogout()
    }

    function renderHeader() {
        return (
            <Header
                token={theprops.token}
                meta={theprops.meta}
                theme={theprops.classes}
            >
                <div
                    style={{
                        display: "flex",
                        flexWrap: "nowrap",
                        flexFlow: "row 1",
                        justifyContent: "space-between",
                    }}
                >
                    <div className={theprops.classes.logoCtr}>
                        {theprops.token.value ? (
                            <NavLink
                                className={theprops.classes.headerLink}
                                role="button"
                                to={routes.HOME}
                            >
                                <span className={theprops.classes.logo}>
                                    AgroET
                                </span>
                                <span className={theprops.classes.logoVersion}>
                                    v. {theprops.meta.version}
                                </span>
                            </NavLink>
                        ) : (
                            <NavLink
                                className={theprops.classes.headerLink}
                                role="button"
                                to={routes.HOME}
                            >
                                <span className={theprops.classes.logo}>
                                    AgroET
                                </span>
                                <span className={theprops.classes.logoVersion}>
                                    v. {theprops.meta.version}
                                </span>
                            </NavLink>
                        )}
                    </div>
                    <div
                        style={{
                            display: "flex",
                            flexWrap: "nowrap",
                            flexFlow: "row 1",
                            justifyContent: "space-between",
                        }}
                    >
                        <UserSettings
                            actions={{
                                logout,
                            }}
                            defaultRoute={routes.SELECT_AOI}
                            routes={routes}
                            theme={theprops.classes}
                            user={theprops.user}
                        />
                    </div>
                </div>
            </Header>
        )
    }

    function renderContextBar() {
        return (
            <ContextBar
                items={theprops.contextBar.items}
                state={theprops}
                theme={classes}
                token={theprops.token}
                onClickContextBarItem={theprops.actions.onClickContextBarItem}
            />
        )
    }

    if (!theprops.token.value) {
        // Render loading page until the server state is available
        return (
            <div className={classes.root}>
                {renderHeader()}
                {renderContextBar()}
                <div className={classes.loading}>
                    <FontAwesomeIcon icon={"cog"} spin size={"3x"} />
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        )
    }

    const hasClimate =
        theprops.climate.stations && theprops.climate.stations.length > 0

    if (!theprops.selectImage.scene) {
        // No image, make sure we are on image selection
        const routesCheck = [
            routes.SELECT_AOI,
            routes.PROCESS_SCENE,
            routes.SELECT_CLIMATE,
        ]
        if (routesCheck.includes(location.pathname))
            return <Redirect to={routes.SELECT_IMAGE} />
    } else if (!hasClimate) {
        const routesCheck = [routes.PROCESS_SCENE, routes.SELECT_CLIMATE]
        if (routesCheck.includes(location.pathname))
            return <Redirect to={routes.SELECT_AOI} />
    }

    const tocVisible =
        (location.pathname === routes.SELECT_AOI &&
            theprops.selectAOI.visible) ||
        (location.pathname === routes.SELECT_IMAGE &&
            theprops.selectImage.visible) ||
        (location.pathname === routes.SELECT_CLIMATE &&
            theprops.selectClimate.visible) ||
        (location.pathname === routes.PROCESS_OPTIONS &&
            theprops.processOptions.visible) ||
        (location.pathname === routes.PROCESS_SCENE &&
            theprops.processScene.visible) ||
        (location.pathname === routes.SETTINGS && theprops.settings.visible)

    const attributeTables = theprops.attributeTables.attributeTables.map(
        (t) => (
            <AttributeTable
                attributeTable={t}
                onClose={() => theprops.actions.onHideAttributeTable(t.key)}
                key={t.key}
                theme={classes}
                token={theprops.token}
                {...theprops.actions}
            />
        ),
    )

    // Analysis panel is the south panel and will contain the logger and attribute tables
    const analysisPanelContents = [...attributeTables]

    if (catenaMap.current) {
        const climateComponent = (
            <Climate
                key={`Climate (${
                    theprops.selectImage.scene
                        ? theprops.selectImage.scene[
                              // The date for some reason is formatted line YYYY/MM/DD
                              "Acquisition Date"
                          ].replace(/\//g, "-")
                        : ""
                })`}
                climate={theprops.climate}
                catenaMap={catenaMap.current.map}
                onSetStationChecked={theprops.actions.onSetStationChecked}
                theme={classes}
            />
        )
        analysisPanelContents.push(climateComponent)
    }

    const sub = (s) => s.substr(1, s.length - 2)
    const mapPath = `/(${sub(routes.SELECT_IMAGE)}|${sub(
        routes.SELECT_AOI,
    )}|${sub(routes.SELECT_CLIMATE)}|${sub(routes.PROCESS_OPTIONS)}|${sub(
        routes.PROCESS_SCENE,
    )}|${sub(routes.SETTINGS)})/`

    const overrides = {
        MuiInput: {
            root: {
                "& input::-webkit-clear-button, & input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button": {
                    display: "none",
                },
            },
            underline: {
                "&:after": {
                    transition: "none",
                },
            },
        },
    }

    const theMuiTheme = customMuiTheme({ colors, overrides })
    return (
        <ErrorBoundary>
            <MuiThemeProvider theme={theMuiTheme}>
                <div className={classes.root}>
                    {renderHeader()}
                    {renderContextBar()}
                    <Route
                        path={routes.HOME}
                        exact
                        render={() => (
                            <Breadcrumbs
                                header={{
                                    id: ids.SUBHEADER_DOCS,
                                    name: "AgroET",
                                }}
                                items={[]}
                                theme={classes}
                            />
                        )}
                    />
                    <Route
                        path={routes.INSTRUCTIONS}
                        render={() => (
                            <Breadcrumbs
                                header={{
                                    id: ids.SUBHEADER_DOCS,
                                    name: "AgroET",
                                }}
                                items={[]}
                                theme={classes}
                            />
                        )}
                    />
                    <Route
                        path={routes.HOME}
                        exact
                        render={() => (
                            <Instructions
                                location={theprops.location}
                                theme={classes}
                                token={theprops.token}
                            />
                        )}
                    />
                    <Route
                        path={routes.HOME}
                        exact
                        render={() => (
                            <TableOfContents
                                items={theprops.tableOfContents.items}
                                location={theprops.location}
                                theme={classes}
                                token={theprops.token}
                            />
                        )}
                    />
                    <Route
                        path={routes.INSTRUCTIONS}
                        render={() => (
                            <Instructions
                                theme={classes}
                                token={theprops.token}
                            />
                        )}
                    />
                    <Route
                        path={routes.INSTRUCTIONS}
                        render={() => (
                            <TableOfContents
                                items={theprops.tableOfContents.items}
                                theme={classes}
                                token={theprops.token}
                            />
                        )}
                    />

                    {tocVisible && (
                        <Route
                            path={routes.SELECT_IMAGE}
                            render={() => (
                                <Breadcrumbs
                                    header={{
                                        id: ids.SELECT_IMAGE_SUBHEADER,
                                        name: "Select LANDSAT",
                                    }}
                                    items={[]}
                                    theme={classes}
                                />
                            )}
                        />
                    )}
                    {tocVisible && catenaMap.current && (
                        <Route
                            path={routes.SELECT_IMAGE}
                            render={() => (
                                <ErrorBoundary>
                                    <SelectImage
                                        actions={theprops.actions}
                                        catenaMap={catenaMap.current.map}
                                        climate={theprops.climate}
                                        hideBQA={theprops.actions.hideBQA}
                                        history={theprops.history}
                                        location={theprops.location}
                                        mapsources={theprops.mapsources}
                                        maybeFindImagery={
                                            theprops.actions.maybeFindImagery
                                        }
                                        onProcessScene={
                                            theprops.actions.onProcessScene
                                        }
                                        onSetScene={theprops.actions.onSetScene}
                                        resetSearch={
                                            theprops.actions.resetSearch
                                        }
                                        selectAOI={theprops.selectAOI}
                                        selectImage={theprops.selectImage}
                                        setSearchDate={
                                            theprops.actions.setSearchDate
                                        }
                                        setSearchFeature={
                                            theprops.actions.setSearchFeature
                                        }
                                        setSearchPath={
                                            theprops.actions.setSearchPath
                                        }
                                        setSearchRow={
                                            theprops.actions.setSearchRow
                                        }
                                        theme={classes}
                                        updateImagery={
                                            theprops.actions.updateImagery
                                        }
                                    />
                                </ErrorBoundary>
                            )}
                        />
                    )}

                    {tocVisible && (
                        <Route
                            path={routes.SELECT_AOI}
                            render={() => (
                                <Breadcrumbs
                                    header={{
                                        id: ids.SELECT_AOI_SUBHEADER,
                                        name: "Select AOI",
                                    }}
                                    items={[]}
                                    theme={classes}
                                />
                            )}
                        />
                    )}

                    {tocVisible && (
                        <Route
                            path={routes.SELECT_CLIMATE}
                            render={() => (
                                <Breadcrumbs
                                    header={{
                                        id: ids.SELECT_CLIMATE_SUBHEADER,
                                        name: "Select Climate",
                                    }}
                                    items={[]}
                                    theme={classes}
                                />
                            )}
                        />
                    )}
                    {catenaMap.current && (
                        <Route
                            path={routes.SELECT_CLIMATE}
                            render={() => (
                                <SelectClimate
                                    catenaMap={catenaMap.current.map}
                                    climate={theprops.climate}
                                    selectClimate={theprops.selectClimate}
                                    setHasHotAndColdAOIs={
                                        theprops.actions.setHasHotAndColdAOIs
                                    }
                                    setHasHotAndColdPoints={
                                        theprops.actions.setHasHotAndColdPoints
                                    }
                                    setPoints={theprops.actions.setPoints}
                                    theme={classes}
                                    uploadResetClimate={
                                        theprops.actions.uploadResetClimate
                                    }
                                />
                            )}
                        />
                    )}

                    {tocVisible && (
                        <Route
                            path={routes.PROCESS_OPTIONS}
                            render={() => (
                                <Breadcrumbs
                                    header={{
                                        id: ids.PROCESS_OPTIONS,
                                        name: "Process Options",
                                    }}
                                    items={[]}
                                    theme={classes}
                                />
                            )}
                        />
                    )}
                    {tocVisible && catenaMap.current && (
                        <Route
                            path={routes.PROCESS_OPTIONS}
                            render={() => (
                                <ErrorBoundary>
                                    <ProcessOptions
                                        catenaMap={catenaMap.current.map}
                                        climate={theprops.climate}
                                        mapsources={
                                            theprops.mapsources.mapsources
                                        }
                                        onChangeProps={
                                            theprops.actions
                                                .onUserLayersPropsChange
                                        }
                                        processOptions={theprops.processOptions}
                                        selectClimate={theprops.selectClimate}
                                        selectAOI={theprops.selectAOI}
                                        selectImage={theprops.selectImage}
                                        setHasHotAndColdAOIs={
                                            theprops.actions
                                                .setHasHotAndColdAOIs
                                        }
                                        setHasHotAndColdPoints={
                                            theprops.actions
                                                .setHasHotAndColdPoints
                                        }
                                        setPoints={theprops.actions.setPoints}
                                        setRunOption={
                                            theprops.actions.setRunOption
                                        }
                                        theme={classes}
                                        uploadResetClimate={
                                            theprops.actions.uploadResetClimate
                                        }
                                    />
                                </ErrorBoundary>
                            )}
                        />
                    )}
                    {tocVisible && (
                        <Route
                            path={routes.PROCESS_SCENE}
                            render={() => (
                                <Breadcrumbs
                                    header={{
                                        id: ids.PROCESS_SCENE_SUBHEADER,
                                        name: "Process Scene",
                                    }}
                                    items={[]}
                                    theme={classes}
                                />
                            )}
                        />
                    )}
                    {tocVisible && catenaMap.current && (
                        <Route
                            path={routes.PROCESS_SCENE}
                            render={() => (
                                <ErrorBoundary>
                                    <ProcessScene
                                        catenaMap={catenaMap.current.map}
                                        climate={theprops.climate}
                                        mapsources={
                                            theprops.mapsources.mapsources
                                        }
                                        onProcessScene={
                                            theprops.actions.onProcessScene
                                        }
                                        onChangeProps={
                                            theprops.actions
                                                .onUserLayersPropsChange
                                        }
                                        onDownload={
                                            theprops.actions.onUserLayerDownload
                                        }
                                        processScene={theprops.processScene}
                                        selectClimate={theprops.selectClimate}
                                        selectImage={theprops.selectImage}
                                        selectAOI={theprops.selectAOI}
                                        setHasHotAndColdAOIs={
                                            theprops.actions
                                                .setHasHotAndColdAOIs
                                        }
                                        setHasHotAndColdPoints={
                                            theprops.actions
                                                .setHasHotAndColdPoints
                                        }
                                        setPoints={theprops.actions.setPoints}
                                        setRunOption={
                                            theprops.actions.setRunOption
                                        }
                                        theme={classes}
                                        uploadResetClimate={
                                            theprops.actions.uploadResetClimate
                                        }
                                    />
                                </ErrorBoundary>
                            )}
                        />
                    )}

                    <Route
                        path={routes.SETTINGS}
                        exact
                        render={() => (
                            <Breadcrumbs
                                header={{
                                    id: ids.SETTINGS,
                                    name: "Settings",
                                }}
                                items={[]}
                                theme={classes}
                            />
                        )}
                    />
                    <Route
                        path={routes.SETTINGS}
                        render={() => (
                            <Settings
                                settings={theprops.settings}
                                updateSettings={theprops.actions.updateSettings}
                            />
                        )}
                    />

                    <Route
                        path={mapPath}
                        render={() => (
                            <ResetMap
                                baseLayer={theprops.mapBaseLayer.baseLayer}
                                contextBarClosed={!tocVisible}
                                extent={theprops.map.extent}
                                map={theprops.map}
                                mapScale={theprops.map.mapView.mapScale}
                                mapsources={theprops.mapsources.mapsources}
                                mapToolbarClosed
                                theme={classes}
                                ref={catenaMap}
                                {...theprops.actions}
                            />
                        )}
                    />

                    {catenaMap.current && (
                        <Route
                            path={mapPath}
                            render={() => (
                                <UserLayersContainer
                                    actions={theprops.actions}
                                    baseImageURL={"/static/er2_reset/dist"}
                                    er2Map={catenaMap.current.map}
                                    geoBar={theprops.geoBar}
                                    identify={theprops.identify}
                                    map={theprops.map}
                                    mapBaseLayer={theprops.mapBaseLayer}
                                    mapsources={theprops.mapsources}
                                    mapToolbar={theprops.mapToolbar}
                                    publicSites={theprops.publicSites}
                                    services={theprops.services}
                                    theme={classes}
                                    tocVisible={tocVisible}
                                    token={theprops.token}
                                />
                            )}
                        />
                    )}

                    {catenaMap.current && (
                        <Route
                            path={mapPath}
                            render={() => {
                                const ret = [
                                    <WindowState
                                        key={"window"}
                                        onChangeWindowState={
                                            theprops.actions
                                                .setTabbedPanelWindowState
                                        }
                                        opacity={theprops.theme.opacity}
                                        theme={classes}
                                        tocVisible={tocVisible}
                                        type="south"
                                        windowState={
                                            theprops.tabbedPanel.windowState
                                        }
                                    />,
                                ]
                                if (
                                    theprops.tabbedPanel.windowState !==
                                    windowStates.minimized
                                ) {
                                    ret.push(
                                        <ErrorBoundary key={"tabbedPanel"}>
                                            <TabbedPanel
                                                heightOffset={
                                                    theprops.theme
                                                        .analysisPanelOffset
                                                }
                                                maptoolbarVisible={false}
                                                onResize={
                                                    theprops.actions
                                                        .onResizeAnalysisPanel
                                                }
                                                onSetTabbedPanelKey={
                                                    theprops.actions
                                                        .onSetTabbedPanelKey
                                                }
                                                tabbedPanel={
                                                    theprops.tabbedPanel
                                                }
                                                theme={classes}
                                                tocVisible={tocVisible}
                                            >
                                                {analysisPanelContents}
                                            </TabbedPanel>
                                        </ErrorBoundary>,
                                    )
                                }
                                return ret
                            }}
                        />
                    )}
                    <Route
                        path={routes.USER}
                        render={() => (
                            <ErrorBoundary>
                                <User
                                    canDelete
                                    onRemove={theprops.actions.onRemoveUser}
                                    onUpdate={
                                        theprops.actions.onRegistrationUpdate
                                    }
                                    theme={classes}
                                    tocVisible={tocVisible}
                                    user={theprops.user}
                                />
                                <Breadcrumbs
                                    header={{
                                        id: "user_breadcrumb",
                                        name: "User",
                                    }}
                                    items={[]}
                                    theme={classes}
                                />
                            </ErrorBoundary>
                        )}
                    />
                </div>
            </MuiThemeProvider>
        </ErrorBoundary>
    )
}

App.defaultProps = {
    match: { params: {} },
    mapToolbar: { items: [] },
    tableOfContents: { items: [] },
}

const mapStateToProps = (state) => ({
    attributeTables: state.attributeTables,
    climate: state.climate,
    contextBar: state.contextBar,
    geoBar: state.geoBar,
    identify: state.identify,
    logger: state.logger,
    map: state.map,
    mapBaseLayer: state.mapBaseLayer,
    mapsources: state.mapsources,
    mapToolbar: state.mapToolbar,
    meta: state.meta,
    processOptions: state.processOptions,
    processScene: state.processScene,
    publicSites: state.publicSites,
    selectAOI: state.selectAOI,
    selectClimate: state.selectClimate,
    selectImage: state.selectImage,
    serverState: state.serverState,
    services: state.services,
    settings: state.settings,
    tabbedPanel: state.tabbedPanel,
    tableOfContents: state.tableOfContents,
    theme: state.theme,
    token: state.token,
    user: state.user,
})

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(Actions, dispatch),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App))
