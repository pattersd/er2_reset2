// @flow
import React from "react"
import { withRouter } from "react-router-dom"
import Button from "@material-ui/core/Button"
import FormControl from "@material-ui/core/FormControl"
import InputLabel from "@material-ui/core/InputLabel"
import MenuItem from "@material-ui/core/MenuItem"
import Select from "@material-ui/core/Select"
import TextField from "@material-ui/core/TextField"
import { Step, Stepper, StepContent, StepLabel } from "@material-ui/core"
import {
    addMonths,
    format,
    getYear,
    isValid,
    parseISO,
    subDays,
} from "date-fns"
import Map from "@owsi/catena/er2_map/src/ol_map"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import type { MapsourcesType } from "@owsi/catena/er2_map_userlayers/js/reducers/mapsources"
import * as routes from "../constants/routes"
import Theme from "../styles"
import type { SelectImageType } from "../reducers/select_image"
import type { SelectAOItype } from "../reducers/selectAOI"
import type { ClimateType } from "../reducers/climate"
import SelectAOI from "./selectAOI"

type PropsType = {
    actions: {
        generateInputs: Function,
        onUserLayersPropsChange: Function,
        setAOI: Function,
        uploadAOI: Function,
    },
    catenaMap: Map,
    climate: ClimateType,
    history: Function,
    location: {
        search: string,
    },
    mapsources: MapsourcesType,
    maybeFindImagery: Function,
    onProcessScene: Function,
    onSetScene: Function,
    selectAOI: SelectAOItype,
    selectImage: SelectImageType,
    setSearchDate: Function,
    setSearchPath: Function,
    setSearchRow: Function,
    setSearchFeature: Function,
    updateImagery: Function,
    theme: Theme,
}

const dateFmt = "yyyy-MM-dd"
const loadingStyle = { display: "inline-block", marginRight: 20 }

function SelectImage(props: PropsType) {
    function formatImageEntry(i) {
        return (
            <span>
                {i.entityId.substring(0, 3)}
                &nbsp;
                <b>{i.datetime}</b>
                &nbsp; Clouds <b>{i["eo:cloud_cover"].toFixed(0)}%</b>
                &nbsp; Row <b>{parseInt(i["WRS Row"])}</b>
                &nbsp; Path <b>{parseInt(i["WRS Path"])}</b>
                &nbsp;
                {i["Collection Category"]}
            </span>
        )
    }

    const [endDate, setEndDate] = React.useState("")
    const [isInit, setIsInit] = React.useState(true)
    const [startDate, setStartDate] = React.useState("")

    const generateInputs = () => {
        props.actions.generateInputs(
            props.selectAOI.aoi,
            props.selectImage.scene,
        )
    }

    React.useEffect(() => {
        // Turn off all layers if this is the first time the mapsources have been loaded.
        if (isInit) {
            props.mapsources.mapsources.forEach((m) => {
                m.layers.forEach((l) => {
                    if (l.visible && l.name.indexOf("BQA") < 0) {
                        props.actions.onUserLayersPropsChange(l.mapsource, l, {
                            visible: false,
                        })
                    }
                })
                setIsInit(false)
            })
        }
    }, [props.mapsources])

    React.useEffect(() => {
        if (props.selectImage.scene && props.climate.stations.length === 0) {
            generateInputs()
        }
    }, [props.selectImage.scene])

    React.useEffect(() => {
        setStartDate(props.selectImage.startDate || "")
    }, [props.selectImage.startDate])

    React.useEffect(() => {
        setEndDate(props.selectImage.endDate || "")
    }, [props.selectImage.endDate])

    const downloadClimate = () => {
        window.open("/er2_reset/download_climate/")
    }

    const selectImage = (e) => {
        const currentScene = props.selectImage.imageryResults.find(
            (i) => i.id === e.target.value,
        )
        props.onSetScene(currentScene)
    }

    function updateDates(start, end) {
        props.setSearchDate("startDate", start)
        props.setSearchDate("endDate", end)
        props.updateImagery([])
    }

    function updateStartDate() {
        let oStartDate = parseISO(startDate)
        let newStartDate = startDate
        // Start date must be at least a month prior
        const oMinStartDate = subDays(new Date(), 10)

        if (isValid(oStartDate)) {
            if (getYear(oStartDate) < 1986) {
                newStartDate = "1986-01-01"
                setStartDate(newStartDate)
                oStartDate = parseISO(newStartDate)
            } else if (oStartDate > oMinStartDate) {
                newStartDate = format(oMinStartDate, dateFmt)
                setStartDate(newStartDate)
            }

            // Automatically update the end date to one month after the start date
            let oEndDate = addMonths(oStartDate, 1)
            if (oEndDate > new Date()) {
                oEndDate = new Date()
            }
            const newEndDate = format(oEndDate, dateFmt)
            setEndDate(newEndDate)
            updateDates(newStartDate, newEndDate)
        } else {
            updateDates("", "")
        }
        props.maybeFindImagery()
    }

    function updateEndDate() {
        let newEndDate = endDate
        const dt = parseISO(endDate)
        if (isValid(dt)) {
            if (getYear(dt) < 1986) {
                newEndDate = "1986-02-01"
                setEndDate(newEndDate)
            }
        }
        updateDates(startDate, newEndDate)
        props.maybeFindImagery()
    }

    const onClickAdvancedOptions = () => {
        props.history.push(routes.PROCESS_OPTIONS)
    }

    const onClickRun = () => {
        props.history.push(routes.PROCESS_SCENE)
        props.onProcessScene()
    }

    const onSetAOI = (aoi) => {
        props.actions.setAOI(aoi)
        props.setSearchFeature(aoi)
        props.maybeFindImagery()
    }

    const onChangeStartDate = (e) => {
        setStartDate(e.target.value)
    }

    function renderSearchDates() {
        return (
            <div
                style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                }}
            >
                <TextField
                    disabled={
                        props.selectImage.isRunning || props.selectAOI.isRunning
                    }
                    InputLabelProps={{ shrink: true }}
                    label="Start Date"
                    onBlur={() => updateStartDate()}
                    onChange={onChangeStartDate}
                    type="date"
                    value={startDate}
                />
                &nbsp;
                <TextField
                    disabled={
                        props.selectImage.isRunning || props.selectAOI.isRunning
                    }
                    InputLabelProps={{ shrink: true }}
                    label="End Date"
                    onBlur={() => updateEndDate()}
                    onChange={(e) => setEndDate(e.target.value)}
                    type="date"
                    value={endDate}
                />
            </div>
        )
    }

    function renderSearchAOI() {
        return (
            <SelectAOI
                catenaMap={props.catenaMap}
                selectAOI={props.selectAOI}
                selectImage={props.selectImage}
                setAOI={onSetAOI}
                theme={props.theme}
                uploadAOI={props.actions.uploadAOI}
            />
        )
    }

    function renderSelectImage(disabled) {
        const images = props.selectImage.imageryResults
        if (images.length) {
            const isInList =
                props.selectImage.scene &&
                images.find((s) => props.selectImage.scene.id === s.id)
            const value =
                isInList && props.selectImage.scene
                    ? props.selectImage.scene.id
                    : ""
            return (
                <FormControl
                    disabled={disabled}
                    fullWidth
                    style={{
                        marginTop: 10,
                    }}
                >
                    <InputLabel htmlFor={"image-helper"}>
                        Select a Scene
                    </InputLabel>
                    <Select
                        disabled={
                            props.selectImage.isRunning ||
                            props.selectAOI.isRunning
                        }
                        labelId={"image-helper"}
                        onChange={selectImage}
                        title={value}
                        value={value}
                    >
                        {images.map((i) => (
                            <MenuItem
                                key={i.id}
                                value={i.id}
                                title={i.entityId}
                            >
                                {formatImageEntry(i)}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            )
        }
        return null
    }

    function renderCreateInputs() {
        if (props.selectAOI.isRunning) {
            return (
                <div
                    style={{
                        textAlign: "center",
                        margin: 10,
                        verticalAlign: "middle",
                    }}
                >
                    <span style={loadingStyle}>
                        <FontAwesomeIcon icon={"spinner"} size={"2x"} spin />
                    </span>
                    <span>Generating climate...</span>
                </div>
            )
        }
        return (
            <div style={{ textAlign: "center", margin: "8px" }}>
                <div
                    style={{ display: "flex", justifyContent: "space-around" }}
                >
                    <Button
                        disabled={
                            props.selectImage.isRunning ||
                            props.selectAOI.isRunning
                        }
                        onClick={generateInputs}
                        variant="contained"
                    >
                        Extract
                    </Button>
                </div>
            </div>
        )
    }

    function renderRunButtons() {
        if (
            props.selectAOI.aoi &&
            props.selectImage.scene &&
            props.climate.stations.length > 0
        ) {
            return (
                <div style={{ textAlign: "center", margin: "8px" }}>
                    <div
                        style={{
                            display: "flex",
                            justifyContent: "space-around",
                        }}
                    >
                        <Button onClick={onClickRun} variant="contained">
                            Run AgroET
                        </Button>
                        <Button
                            onClick={onClickAdvancedOptions}
                            variant="contained"
                        >
                            Advanced Options
                        </Button>
                    </div>
                </div>
            )
        }
        return null
    }

    function hasDateRange() {
        const start = parseISO(props.selectImage.startDate)
        const end = parseISO(props.selectImage.endDate)
        return isValid(start) && isValid(end) && start < end
    }
    function hasImageryResults() {
        return (
            props.selectImage.imageryResults === null ||
            props.selectImage.imageryResults.length === 0
        )
    }
    const steps = [
        // Label, render, completed, active (whether it should be displayed or not)
        ["Set Date Range", renderSearchDates, hasDateRange(), true],
        [
            "Set Area of Interest (AOI)",
            renderSearchAOI,
            Boolean(props.selectImage.searchFeature),
            true,
        ],
        [
            "Select Image",
            renderSelectImage,
            Boolean(props.selectImage.scene),
            props.selectImage.searchFeature && hasDateRange(),
        ],
        // ['Extract Climate', renderCreateInputs, props.climate.stations.length > 0, !props.selectImage.scene],
    ]
    return (
        <div className={props.theme.controlBar} style={{ marginTop: 10 }}>
            <Stepper orientation="vertical">
                {steps.map(([label, renderFunction, completed, active], i) => (
                    <Step active={active} completed={completed} key={label}>
                        <StepLabel>{label}</StepLabel>
                        <StepContent>{renderFunction()}</StepContent>
                    </Step>
                ))}
            </Stepper>

            {props.selectImage.isRunning && (
                <div
                    style={{
                        textAlign: "center",
                        margin: 10,
                        verticalAlign: "middle",
                    }}
                >
                    <span style={loadingStyle}>
                        <FontAwesomeIcon icon={"spinner"} size={"2x"} spin />
                    </span>
                    {props.selectImage.imageryResults === null && (
                        <span>Finding LANDSAT scenes...</span>
                    )}
                    {props.selectImage.imageryResults &&
                        props.selectImage.imageryResults.length > 0 && (
                            <span>Loading LANDSAT scene...</span>
                        )}
                </div>
            )}
            {props.selectAOI.isRunning && (
                <div
                    style={{
                        textAlign: "center",
                        margin: 10,
                        verticalAlign: "middle",
                    }}
                >
                    <span style={loadingStyle}>
                        <FontAwesomeIcon icon={"spinner"} size={"2x"} spin />
                    </span>
                    <span>Loading Climate and DEM...</span>
                </div>
            )}
            {renderRunButtons()}
            {props.selectImage.scene && !props.selectImage.isRunning && (
                <div>
                    <img
                        src={props.selectImage.scene.thumbnail.href}
                        style={{ width: "100%", height: "100%" }}
                        alt="scene"
                    />
                </div>
            )}
        </div>
    )
}

export default withRouter(SelectImage)
