// @flow
import React from "react";
import ResizableTable from "@owsi/catena/er2_map_userlayers/js/components/resizable_table";
import Checkbox from "@material-ui/core/Checkbox";
import OlStyle from "ol/style/Style";
import OlCircle from "ol/style/Circle";
import OlFill from "ol/style/Fill";
import OlStroke from "ol/style/Stroke";
import OlText from "ol/style/Text";
import OlVectorLayer from "ol/layer/Vector";
import OlVectorSource from "ol/source/Vector";
import { olUtils } from "@owsi/catena/er2_ui";
import Map from "@owsi/catena/er2_map/src/ol_map";
import type { ClimateType } from "../reducers/climate";

type PropsType = {
  catenaMap: Map,
  climate: ClimateType,
  onSetStationChecked: Function,
  theme: { controlBar: string },
};

function getStyle(feat, res) {
  return new OlStyle({
    image: new OlCircle({
      radius: 6,
      stroke: new OlStroke({
        color: "orange",
        width: 2,
      }),
      fill: new OlFill({
        color: "red",
      }),
    }),
    text: new OlText({
      font: '14px "Roboto Condensed", sans-serif',
      fill: new OlFill({ color: "green" }),
      offsetX: 0,
      offsetY: 20,
      stroke: new OlStroke({ color: "white", width: 2 }),
      text: feat.get("name"),
    }),
  });
}

function Climate(props: PropsType) {
  const [climateLayer, setClimateLayer] = React.useState();

  const olmap = props.catenaMap.olmap;

  function updateClimateLayer() {
    let source;
    if (climateLayer) {
      source = climateLayer.getSource();
      source.clear();
    } else {
      source = new OlVectorSource();
      const cl = new OlVectorLayer({ source, style: getStyle });
      olmap.addLayer(cl);
      setClimateLayer(cl);
    }

    const feats = olUtils.readFeatures(props.climate.stations);
    if (feats.length) {
      source.addFeatures(feats);
      // const extent = source.getExtent()
      // olmap.getView().fit(extent, olmap.getSize())
    }
  }

  React.useEffect(() => {
    updateClimateLayer();
  }, [props.climate]);

  const columns = React.useMemo(
    () => [
      {
        Header: "Options",
        Cell: ({ row }) => (
          <div className={props.theme.cell}>
            <Checkbox
              checked={!row.original.ignore}
              onChange={(e) =>
                props.onSetStationChecked(row.original.id, e.target.checked)
              }
            />
          </div>
        ),
      },
      {
        Header: "Station",
        accessor: "name",
        Cell: ({ cell }) => (
          <div className={props.theme.cell}>{cell.value}</div>
        ),
      },
      {
        Header: "ETr (mm)",
        accessor: "etr_mm",
        Cell: ({ cell }) => (
          <div className={props.theme.cell}>
            {cell.value ? cell.value.toFixed(3) : ""}
          </div>
        ),
      },
      {
        Header: "ETr Hourly (mm)",
        accessor: "etr_hourly_mm",
        Cell: ({ cell }) => (
          <div className={props.theme.cell}>
            {cell.value ? cell.value.toFixed(3) : ""}
          </div>
        ),
      },
      {
        Header: "TAve Hourly (C)",
        accessor: "tave_hourly_c",
        Cell: ({ cell }) => (
          <div className={props.theme.cell}>
            {cell.value ? cell.value.toFixed(3) : ""}
          </div>
        ),
      },
      {
        Header: "Wind Run (mi)",
        accessor: "wrun_mile_day",
        Cell: ({ cell }) => (
          <div className={props.theme.cell}>
            {cell.value ? cell.value.toFixed(3) : ""}
          </div>
        ),
      },
    ],
    [props.climate]
  );

  const stations = React.useMemo(
    () => props.climate.stations.map((s) => ({ ...s.properties, id: s.id })),
    [props.climate]
  );
  return (
    <div className={props.theme.controlBar}>
      <ResizableTable
        data={stations}
        columns={columns}
        height={props.height}
        theme={props.theme}
      />
    </div>
  );
}

export default Climate;
