// @flow
import * as React from "react";
import { TokenNavLink } from "@owsi/catena/er2_ui";

type Props = {
  idx: number,
  item: { id: string, link: string, name: string },
  active: boolean,
  onClick: Function,
  theme: { active: string, link: string, tableOfContentsBarItem: string },
  token: { value: string },
};

// Displays buttons or links associated with a table of contents
function TableOfContentsBarItem(props: Props) {
  const classNames = `${props.theme.tableOfContentsBarItem} ${props.theme.link}
        ${props.active ? props.theme.active : ""}`;
  return (
    <TokenNavLink
      className={classNames}
      role="button"
      tabIndex={props.idx}
      to={props.item.link || "#"}
      token={props.token}
      onClick={() => props.onClick(props.item.id)}
    >
      {props.item.name}
    </TokenNavLink>
  );
}

export default TableOfContentsBarItem;
