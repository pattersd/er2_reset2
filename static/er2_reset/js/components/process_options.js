// @flow
import React from "react";
import { Checkbox, FormControlLabel } from "@material-ui/core";
import Map from "@owsi/catena/er2_map/src/map";
import Help from "@owsi/catena/er2_ui/components/component_help";
import type { MapsourcesType } from "@owsi/catena/er2_map_userlayers/js/reducers/mapsources";
import SelectClimate from "./select_climate";
import Theme from "../styles";
import type { SelectImageType } from "../reducers/select_image";
import type { ClimateType } from "../reducers/climate";
import type { SelectClimateType } from "../reducers/select_climate";
import type { ProcessOptionsType } from "../reducers/process_options";

type PropsType = {
  catenaMap: Map,
  climate: ClimateType,
  mapsources: MapsourcesType,
  processOptions: ProcessOptionsType,
  selectClimate: SelectClimateType,
  selectImage: SelectImageType,
  setHasHotAndColdAOIs: Function,
  setHasHotAndColdPoints: Function,
  setPoints: Function,
  onChangeProps: Function,
  setRunOption: Function,
  theme: Theme,
  uploadResetClimate: Function,
};

function ProcessOptions(props: PropsType) {
  const setMode = (opt) => (e) => {
    props.setRunOption(opt, e.target.checked);
  };

  const { options } = props.processOptions;
  const optionKeys = Object.keys(options);
  const isLS7 =
    props.selectImage.scene["Spacecraft Identifier"] === "LANDSAT_7";

  return (
    <div className={props.theme.controlBar}>
      <div style={{ textAlign: "left", marginLeft: 10, marginTop: "20px" }}>
        <fieldset style={{ borderRadius: 6 }}>
          <legend className={props.theme.AOIcontainer}>Climate Options</legend>
          <SelectClimate
            catenaMap={props.catenaMap}
            climate={props.climate}
            selectClimate={props.selectClimate}
            setHasHotAndColdAOIs={props.setHasHotAndColdAOIs}
            setHasHotAndColdPoints={props.setHasHotAndColdPoints}
            setPoints={props.setPoints}
            theme={props.theme}
            uploadResetClimate={props.uploadResetClimate}
          />
        </fieldset>
        <fieldset style={{ borderRadius: 6 }}>
          <legend className={props.theme.AOIcontainer}>Run Options</legend>
          {optionKeys.map((opt) => (
            <Help description={options[opt].help} key={opt} theme={props.theme}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={options[opt].value}
                    onChange={setMode(opt)}
                    color="primary"
                  />
                }
                disabled={opt.indexOf("LS7") === 0 && !isLS7}
                label={options[opt].label}
              />
            </Help>
          ))}
        </fieldset>
      </div>
    </div>
  );
}

export default ProcessOptions;
