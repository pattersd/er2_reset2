// @flow
import * as React from 'react'
import { css } from 'aphrodite'
import { NavLink } from 'react-router-dom'
import { styles } from '../styles/root'
import stylesFicus from '../styles/ficus_style'

export type MapToolbarItemType = {
    active: boolean,
    icon: { default: string, active?: string },
    id: string,
    link: string,
    name: string,
    position?: string,
}
type Props = {
    idx: number,
    item: MapToolbarItemType,
    onClick: Function,
}

function MapToolbarItem(props: Props) {
    const classNames = [styles.mapToolbarItem]
    if (props.item.active) classNames.push(styles.active)
    const iconClass = props.item.active && props.item.icon.active ?
        props.item.icon.active :
        props.item.icon.default
    return (
        <div>
            {props.item.link ?
                <NavLink
                    className={css(classNames)}
                    role="button"
                    tabIndex={props.idx}
                    to={props.item.link}
                >
                    <i className={iconClass} aria-hidden="true" title={props.item.name} />
                </NavLink>
                :
                <div
                    className={css(classNames)}
                    role="button"
                    tabIndex={props.idx}
                    onClick={evt => props.onClick(props.item.id, evt)}
                >
                    <i className={iconClass} aria-hidden="true" title={props.item.name} />
                </div>
            }
        </div>
    )
}

export default MapToolbarItem
