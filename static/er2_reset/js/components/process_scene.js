// @flow
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  IconButton,
  LinearProgress,
  Typography,
} from "@material-ui/core";
import Map from "@owsi/catena/er2_map/src/map";
import Help from "@owsi/catena/er2_ui/components/component_help";
import type { MapsourcesType } from "@owsi/catena/er2_map_userlayers/js/reducers/mapsources";
import Theme from "../styles";
import type { SelectAOItype } from "../reducers/selectAOI";
import type { SelectImageType } from "../reducers/select_image";
import type { ProcessSceneType } from "../reducers/process_scene";
import type { ClimateType } from "../reducers/climate";
import type { SelectClimateType } from "../reducers/select_climate";

type PropsType = {
  catenaMap: Map,
  climate: ClimateType,
  mapsources: MapsourcesType,
  onChangeProps: Function,
  onDownload: Function,
  onProcessScene: Function,
  selectClimate: SelectClimateType,
  selectImage: SelectImageType,
  setHasHotAndColdAOIs: Function,
  setHasHotAndColdPoints: Function,
  setPoints: Function,
  processScene: ProcessSceneType,
  selectAOI: SelectAOItype,
  setRunOption: Function,
  theme: Theme,
  uploadResetClimate: Function,
};

function ProcessScene(props: PropsType) {
  function getMapsource(layer) {
    let mapsource;
    props.mapsources.forEach((m) => {
      if (m.name === layer.mapsource) {
        mapsource = m;
      }
    });
    return mapsource;
  }

  const processScene = () => {
    props.onProcessScene();
  };

  const onChangeVisibility = (layer) => (e) => {
    const mapsource = getMapsource(layer);
    props.onChangeProps(mapsource, layer, { visible: e.target.checked });
  };

  const onDownload = (layer) => (e) => {
    const mapsource = getMapsource(layer);
    props.onDownload(mapsource, layer);
  };

  const setMode = (opt) => (e) => {
    props.setRunOption(opt, e.target.checked);
  };

  function renderOutputs() {
    const prefix = props.selectImage.scene.id;

    const getLayerById = (layerName) => {
      let ret = null;
      props.mapsources.forEach((mapsource) => {
        mapsource.layers.forEach((layer) => {
          if (layer.name === layerName) {
            ret = layer;
          }
        });
      });
      return ret;
    };
    function getLayerText(layer) {
      return layer.name;
    }

    if (props.processScene.outputs.length > 0) {
      return (
        <div style={{ marginTop: "20px", fontSize: "smaller" }}>
          <h3>Results for {prefix}</h3>
          {props.processScene.outputs.map((layerName) => {
            const layer = getLayerById(layerName);
            if (layer) {
              return (
                <div key={layerName}>
                  <div className={props.theme.listContainer}>
                    <FormControl>
                      <Checkbox
                        className={props.theme.checkbox}
                        color="secondary"
                        checked={layer.visible}
                        onChange={onChangeVisibility(layer)}
                        title={"Toggle layer visibility"}
                      />
                    </FormControl>
                    <IconButton
                      className={props.theme.button}
                      onClick={onDownload(layer)}
                      title={"Click to download"}
                    >
                      <FontAwesomeIcon
                        icon={"download"}
                        style={{ fontSize: 12 }}
                      />
                    </IconButton>
                    <Typography
                      className={props.theme.label}
                      style={{
                        fontWeight: layer.visible ? "bold" : "normal",
                      }}
                    >
                      {getLayerText(layer)}
                    </Typography>
                  </div>
                </div>
              );
            }
            // No layer found, just return layer name
            return <div key={layerName}>{layerName}</div>;
          })}
        </div>
      );
    }
    return null;
  }

  function renderProgress() {
    let completed = 0;
    if (props.processScene.progress) {
      const progress = props.processScene.progress.forEach((l) => {
        const m = l.match(/(\d+)% of run completed/);
        if (m) {
          completed = parseInt(m[0]);
        }
        if (l.indexOf("Model run is completed") >= 0) {
          completed = 100;
        }
      });
    }
    return (
      <div style={{ margin: 20 }}>
        <LinearProgress variant="determinate" value={completed} />
      </div>
    );
  }

  const canRun =
    !props.selectAOI.isRunning &&
    (!props.selectClimate.hasHotAndColdPoints ||
      (props.selectClimate.hot_pts && props.selectClimate.cold_pts)) &&
    (!props.selectClimate.hasHotAndColdAOIs ||
      (props.selectClimate.hot_aoi && props.selectClimate.cold_aoi));

  // Weed out "Iterator" lines that appear with a crash has occured.
  const progress = props.processScene.progress
    ? props.processScene.progress
        .slice()
        .reverse()
        .filter((l) => l.indexOf("Iterator") < 0)
    : [];
  const { options } = props.processScene;
  const optionKeys = Object.keys(options);
  const isLS7 =
    props.selectImage.scene["Spacecraft Identifier"] === "LANDSAT_7";

  return (
    <div className={props.theme.controlBar}>
      <div style={{ marginTop: 20, textAlign: "center" }}>
        {!props.processScene.isRunning && (
          <Button disabled={!canRun} onClick={processScene} variant="contained">
            Run AgroET
          </Button>
        )}
        {props.processScene.isRunning && (
          <div>
            <span style={{ fontSize: "10px" }}>
              <FontAwesomeIcon icon={"spinner"} spin size={"2x"} />
            </span>
            <span style={{ marginLeft: "6px", display: "inline-block" }}>
              Processing
            </span>
          </div>
        )}
        <div>
          {progress.length > 0 && (
            <div className={props.theme.progressContainer}>
              {progress.map((l, i) => (
                <div key={`${l}-${i}`}>
                  {l.replace("DEBUG:", "").replace("root:", "")}
                </div>
              ))}
            </div>
          )}
          {props.processScene.isRunning && renderProgress()}
        </div>
      </div>
      {!props.selectAOI.isRunning && renderOutputs()}
    </div>
  );
}

export default ProcessScene;
