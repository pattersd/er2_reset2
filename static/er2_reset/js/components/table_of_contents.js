import * as React from 'react'
import { withRouter } from 'react-router-dom'

import TableOfContentsBarItem from './table_of_contents_bar_item'

// @flow
type Props = {
    items: Array<{id: string, name: string }>,
    location: { pathname: string },
    theme: { tableOfContentsBar: string },
    token: { value: string },
}

// Displays a table of contents with a given list of items
function TableOfContents(props: Props) {
    return (
        <div className={props.theme.tableOfContentsBar}>
            {props.items.map((item, idx) => (
                <TableOfContentsBarItem
                    key={item.id}
                    idx={idx}
                    item={item}
                    active={props.location.pathname === item.link}
                    theme={props.theme}
                    token={props.token}
                />)
            )}
        </div>
    )
}

export default withRouter(TableOfContents)