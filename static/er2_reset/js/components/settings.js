// @flow
import React from 'react'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import type { SettingsBarType } from '../reducers/settings'

export type PropsType = {
    settings: SettingsBarType,
    updateSettings: Function,
}

class Settings extends React.Component<PropsType> {
    selectionType = (e) => {
        this.props.updateSettings({ selectionType: e.target.value })
    }

    render() {
        return (
            <div>
                <div
                    style={{ margin: '20px 10px 0 10px' }}
                >
                    <FormControl fullWidth>
                        <InputLabel htmlFor="imageryselection">Imagery Source</InputLabel>
                        <Select
                            value={this.props.settings.selectionType}
                            onChange={this.selectionType}
                            inputProps={{
                                id: 'imageryselection',
                            }}
                        >
                            <MenuItem value={'satsearch'}>Sat-search</MenuItem>
                            <MenuItem value={'ee'}>Earth Explorer</MenuItem>
                        </Select>
                    </FormControl>
                </div>
            </div>
        )
    }
}

export default Settings
