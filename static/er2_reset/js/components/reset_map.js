// @flow
import React from "react"
import Map from "@owsi/catena/er2_map/src/map"
import type { MapType } from "@owsi/catena/er2_map_userlayers/js/reducers/map"
import type { MapsourcesType } from "@owsi/catena/er2_map_userlayers/js/reducers/mapsources"
import { styles } from "../styles"

type Props = {
    baseLayer: string,
    contextBarClosed: boolean,
    map: MapType,
    mapsources: MapsourcesType,
    mapToolbarClosed: boolean,
    needsMapExtentUpdate: boolean,
    needsMapViewUpdate: boolean,
    needsStateRefresh: boolean,
    onUpdateMapView: Function,
    theme: styles,
    updateMapExtentHandled: Function,
    updateMapViewHandled: Function,
}

const ResetMap = React.forwardRef((props: Props, ref) => {
    const classes = [props.theme.mapCtr]
    classes.push(props.theme.analysisPanelClosed, props.theme.headerPanelOpen)
    classes.push(
        props.contextBarClosed
            ? props.theme.contextBarClosed
            : props.theme.contextBarOpen,
    )
    classes.push(
        props.mapToolbarClosed
            ? props.theme.mapToolbarPanelClosed
            : props.theme.mapToolbarPanelOpen,
    )

    return (
        <div className={classes.join(" ")}>
            <Map
                {...props}
                onChangeView={(ex) => props.onUpdateMapView(ex)}
                ref={ref}
            />
        </div>
    )
})

export default ResetMap
