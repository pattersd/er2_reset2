// @flow
import React from "react";
import { useDropzone } from "react-dropzone";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpload } from "@fortawesome/free-solid-svg-icons";
import IconButton from "@material-ui/core/IconButton";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import OlFormatGeojson from "ol/format/GeoJSON";
import OlInteractionDraw, {
  createBox,
  createRegularPolygon,
} from "ol/interaction/Draw";
import OlGeometryType from "ol/geom/GeometryType";
import Map from "@owsi/catena/er2_map/src/ol_map";
import Theme from "../styles";
import type { SelectAOItype } from "../reducers/selectAOI";

type PropsType = {
  catenaMap: Map,
  selectAOI: SelectAOItype,
  selectImage: SelectImageType,
  setAOI: Function,
  theme: Theme,
  uploadAOI: Function,
};

function SelectAOI(props: PropsType) {
  const [aoiVector, setAoiVector] = React.useState();
  const [aoiInteraction, setAoiInteraction] = React.useState();

  const format = new OlFormatGeojson({
    featureProjection: "EPSG:3857",
    dataProjection: "EPSG:4326",
  });

  React.useEffect(() => {
    const source = new VectorSource({ wrapX: false });
    const vl = new VectorLayer({
      source,
      zIndex: 2,
    });
    props.catenaMap.olmap.addLayer(vl);
    setAoiVector(vl);
    return () => {
      props.catenaMap.olmap.removeLayer(vl);
    };
  }, []);

  React.useEffect(() => {
    if (aoiVector && props.selectAOI.aoi) {
      // Convert geojson to feature
      const feats = format.readFeatures(props.selectAOI.aoi);
      aoiVector.getSource().clear();
      aoiVector.getSource().addFeatures(feats);
      if (feats.length > 0) {
        const extent = aoiVector.getSource().getExtent();
        props.catenaMap.olmap
          .getView()
          .fit(extent, props.catenaMap.olmap.getSize());
      }
    }
  }, [aoiVector, props.selectAOI.aoi]);

  function setInteraction(newInteraction) {
    if (aoiInteraction) {
      props.catenaMap.olmap.removeInteraction(aoiInteraction);
    }
    aoiVector.getSource().clear();

    // Map events
    newInteraction.on("drawend", (e) => {
      if (newInteraction) {
        const geojson = format.writeFeaturesObject([e.feature]);
        props.setAOI(geojson);
        props.catenaMap.olmap.removeInteraction(newInteraction);
        setAoiInteraction(null);
      }
    });
    props.catenaMap.olmap.addInteraction(newInteraction);
    setAoiInteraction(newInteraction);
  }

  const selectCircle = () => {
    setInteraction(
      new OlInteractionDraw({
        geometryFunction: createRegularPolygon(40),
        source: aoiVector.getSource(),
        type: OlGeometryType.CIRCLE,
      })
    );
  };

  const selectPolygon = () => {
    setInteraction(
      new OlInteractionDraw({
        source: aoiVector.getSource(),
        type: OlGeometryType.POLYGON,
      })
    );
  };

  const selectRectangle = () => {
    setInteraction(
      new OlInteractionDraw({
        geometryFunction: createBox(),
        source: aoiVector.getSource(),
        type: OlGeometryType.CIRCLE,
      })
    );
  };

  const onDrop = (acceptedFiles, rejectedFiles) => {
    props.uploadAOI(acceptedFiles, rejectedFiles);
  };

  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  return (
    <div style={{ display: "flex", justifyContent: "space-evenly" }}>
      <div style={{ display: "none", margin: "8px" }}>
        <IconButton
          className={props.theme.icon}
          disabled={props.selectImage.isRunning || props.selectAOI.isRunning}
          onClick={selectCircle}
          title="Click on the map where the center of the circle should be located and move the mouse outward. Click on the map once the circle is correct."
          variant="contained"
        >
          <span className={"ms ms-draw-point"} />
        </IconButton>
      </div>
      <div style={{ display: "inline-block", margin: "8px" }}>
        <IconButton
          className={props.theme.icon}
          disabled={props.selectImage.isRunning || props.selectAOI.isRunning}
          onClick={selectRectangle}
          title="Click on the map where one corner of the rectangle should be located and move the mouse to the opposite corner."
          variant="contained"
        >
          <span className={"ms ms-draw-extent"} />
        </IconButton>
      </div>
      <div style={{ display: "inline-block", margin: "8px" }}>
        <IconButton
          className={props.theme.icon}
          disabled={props.selectImage.isRunning || props.selectAOI.isRunning}
          onClick={selectPolygon}
          title="Create the polygon by clicking on the map where each vertex should be located and click the last point twice to end the drawing."
          variant="contained"
        >
          <span className={"ms ms-draw-polygon"} />
        </IconButton>
      </div>
      <div
        style={{ cursor: "pointer", display: "inline-block", margin: "8px" }}
      >
        <div {...getRootProps()}>
          <input {...getInputProps()} />
          <IconButton
            className={props.theme.icon}
            disabled={props.selectImage.isRunning || props.selectAOI.isRunning}
            title={"Upload shapefile with the AOI"}
            variant="contained"
          >
            <FontAwesomeIcon icon={faUpload} />
          </IconButton>
        </div>
      </div>
    </div>
  );
}

export default SelectAOI;
