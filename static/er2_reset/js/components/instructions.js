// @flow
import * as React from "react";
import { withRouter } from "react-router-dom";
import TokenNavLink from "@owsi/catena/er2_ui/components/token_nav_link";
import { parseLocation } from "er2_web_utils";

import * as ids from "../constants/ids";
import * as routes from "../constants/routes";

type Props = {
  location: { path: string },
  theme: { button: string, instructions: string, link: string },
  token: { value: string },
};

// Moves a section of documentation into the application viewport based on the url
function scrollIntoView(location: { path: string }) {
  const parsedLoc = parseLocation(location);
  if (parsedLoc.path.startsWith(routes.INSTRUCTIONS_PURPOSE)) {
    const el = document.getElementById(ids.INSTR_PURPOSE_LINK);
    if (el) el.scrollIntoView();
  } else if (parsedLoc.path.startsWith(routes.INSTRUCTIONS_GETTING_STARTED)) {
    const el = document.getElementById(ids.INSTR_GETTING_STARTED_LINK);
    if (el) el.scrollIntoView();
  } else if (parsedLoc.path.startsWith(routes.INSTRUCTIONS_ADVANCED)) {
    const el = document.getElementById(ids.INSTR_ADVANCED_LINK);
    if (el) el.scrollIntoView();
  }
}

/**
 * eslint-disable react/prefer-stateless-function
 * Displays documentation for the application
 */
class Instructions extends React.Component<Props> {
  componentDidMount() {
    scrollIntoView(this.props.location);
  }

  componentDidUpdate() {
    scrollIntoView(this.props.location);
  }

  render() {
    const tokenClasses = `${this.props.theme.button} ${this.props.theme.link}`;
    return (
      <div className={this.props.theme.instructions}>
        <TokenNavLink
          className={tokenClasses}
          to={routes.INSTRUCTIONS_PURPOSE}
          token={this.props.token}
        >
          <span id={ids.INSTR_PURPOSE_LINK}>Purpose</span>
        </TokenNavLink>
        <p>
          The AgroET remote sensing model implements a raster approach of the
          energy balance to estimate the Evapotranspiration for large areas
          taking into account the data from multiple weather stations. The model
          has the following capabilities:
          <ul>
            <li>Process ET for a user defined Area of Interest (AOI);</li>
            <li>
              Automatically identify high and low ET pixels as a set of anchors
              points to estimate the ET for a region;
            </li>
            <li>
              Use user defined high and/or low ET anchor points rather than
              automatically generate them;
            </li>
            <li>Automatically exclude areas covered by clouds;</li>
            <li>Use user defined areas to exclude due to cloud cover;</li>
            <li>
              If the user selects to use Landsat 7 images the model has the
              option to fill in the gaps due to the scan line error;
            </li>
            <li>
              The user can specify a number of options such as: using a flat or
              variable DEM, specifying the maximum % cloud cover allowed in the
              AOI, and filling the gaps in Landsat 7 images due to the scan line
              error.
            </li>
          </ul>
        </p>
        <p>
          Once the user logs in to AgroET the first screen the GUI displays in
          shown in Figure 1. The screen is divided into three main sections
          which are outlined by three polygons in Figure 1. The first section is
          the model run options and is located in the upper left hand side of
          the screen where three icons are stacked vertically with some options
          to the right of them (red rectangle), the second section is the middle
          of the screen in which a map is displayed with a set of horizontal
          icons on the top right hand corner to control the map (green
          rectangle) and third section is on the upper right hand side of the
          screen where three vertical icons control the GIS layers that are
          displayed and some GIS settings (purple rectangle).
        </p>
        <figure>
          <img
            src="/static/er2_reset/help/image1.jpeg"
            style={{ width: 600 }}
            alt="Figure 1"
          />
          <figcaption>
            Figure 1: Initial Page of AgroET Graphical User Interface (GUI)
          </figcaption>
        </figure>
        <p>
          The purple rectangle on the right hand side of Figure 1 contains three
          icons staked vertically and they allow the user to control the GIS
          layers display in the middle of the GUI and set some of the
          characteristics of the GIS layers. The three icons are shown in Figure
          2 (a).
        </p>
        <TokenNavLink
          className={tokenClasses}
          to={routes.INSTRUCTIONS_GETTING_STARTED}
          token={this.props.token}
        >
          <span id={ids.INSTR_GETTING_STARTED_LINK}>Getting Started</span>
        </TokenNavLink>
        <p>
          The flow of the model is controlled by the three icons on the upper
          left hand side of the screen. The icons represent the home screen, the
          Select LANDSAT screen and the Process Scene options from top to bottom
          respectively.
        </p>
        <div style={{ display: "inline-block", float: "left", margin: 20 }}>
          <img src="/static/er2_reset/help/image2.jpg" />
        </div>
        <p>
          If the user selects the Select LANDSAT icon{" "}
          <img src="/static/er2_reset/help/landsat_icon.png" /> the user is able
          to:
        </p>
        <ol>
          <li>
            set date range for the model to search for the available satellite
            images;
          </li>
          <li>define the area of interest (AOI);</li>
          <li>select the satellite image to process;</li>
          <li>
            extract the weather data for the date of the selected satellite
            image; and
          </li>
          <li>run the model using the default model option values.</li>
        </ol>
        <h3>1. Define the LANDSAT Image Search Period</h3>
        <p>
          The user first sets the range of dates that the model will use to
          search for satellite images (blue circle with a 1 shown in Figure 1a).
          The user first defines the <b>Start Date</b>. The model automatically
          sets the <b>End Date</b> to one month after the <b>Start Date</b>. The
          user can manually modify the <b>End Date</b> set by the model. Once
          the user clicks on either the day, month or year of the{" "}
          <b>Start Date</b> a pop-up window is displayed with the current date
          highlighted as shown below in Figure 1(b). If the user clicks on one
          of the days in the month shown then the start date is populated with
          this date and the end date is populated with one month later and the
          second step will become active as shown in Figure 1(b). If the user
          decides that the selected date is not what they want they can click on
          any of three entries of the <b>Start Date</b> or <b>End Date</b> and
          modify the date. If they click on the month (in the case shown in
          Figure 1(b) if they click on December 2019 then a list of months and
          year will be displayed as shown in Figure 1(d). The user can select
          another month and/or year or scroll up and down with the arrows at the
          top and bottom of the months and years as highlighted in Figure 1(d)
          as shown in Figure 1(d) and select a different month or year or select
          the up and down arrows both for month or year to select a different
          month or year from those displayed on the screen. The user can also
          select the arrows next the month as shown in Figure 1(d) and scroll
          one month at a time backwards or forward.
        </p>
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <td>
                <figure style={{ margin: 0 }}>
                  <img
                    alt="select dates"
                    src="/static/er2_reset/help/image4.JPG"
                    style={{ width: 200 }}
                  />
                  <figcaption>(a)</figcaption>
                </figure>
              </td>
              <td>
                <figure style={{ margin: 0 }}>
                  <img
                    alt="select dates"
                    src="/static/er2_reset/help/image5.JPG"
                    style={{ width: 200 }}
                  />
                  <figcaption>(b)</figcaption>
                </figure>
              </td>
              <td>
                <figure style={{ margin: 0 }}>
                  <img
                    alt="select dates"
                    src="/static/er2_reset/help/image6.JPG"
                    style={{ width: 200 }}
                  />
                  <figcaption>(c)</figcaption>
                </figure>
              </td>
            </tr>
          </tbody>
        </table>
        <figcaption>
          Figure 2: Selecting of the Date Range. a) Initial Date Range
          selection; b) changing the start date (or end date); and c) once a
          date has been selected.
        </figcaption>

        <h3>Define the Area of Interest</h3>
        <p>
          Once the user has selected a range of dates to search for Landsat
          images the second step is to set the Area of Interest (AOI). The user
          can define the AOI by drawing a rectangle or polygon in the map, or by
          uploading a shapefile. The three icons are shown in Figure 3 and
          described below.
        </p>
        <figure>
          <img src="static/er2_reset/help/aoi.png" />
          <figcaption>
            Figure 3: Icons for selecting of the Area of Interest (AOI)
          </figcaption>
        </figure>

        <p>
          Select <img src="static/er2_reset/help/aoi_draw_rectangle.png" /> to
          delineate the AOI as a rectangle. Click on a location and move the
          cursor to the other corner of the rectangle and click again. This
          defines the rectangle that will be used as the AOI (shown in Figure
          4).
        </p>
        <figure>
          <img src="static/er2_reset/help/image9.jpeg" style={{ width: 600 }} />
          <figcaption>
            Figure 5: Delineating the AOI using a user-defined rectangle
          </figcaption>
        </figure>

        <p>
          Select <img src="static/er2_reset/help/aoi_draw_polygon.png" /> to
          delineate the AOI as a polygon. The user clicks on the vertices of the
          polygon and one they reach the last vertex the double click and the
          system closes the polygon (shown in Figure 6).
        </p>
        <figure>
          <img
            src="static/er2_reset/help/image10.jpeg"
            style={{ width: 600 }}
          />
          <figcaption>
            Figure 6: Delineating the AOI by a user defined polygon
          </figcaption>
        </figure>

        <p>
          Select <img src="static/er2_reset/help/aoi_upload.png" /> to define
          the AOI as a shapefile. Once the use selects the upload a shapefile a
          file selection box is displayed and the user selects all the files
          associated with the AOI shapefile as shown in Figure 7.
        </p>
        <figure>
          <img
            src="static/er2_reset/help/image11.jpeg"
            style={{ width: 600 }}
          />
          <figcaption>Figure 7: Using a shapefile to define the AOI</figcaption>
        </figure>

        <h3>Select LANDSAT Image</h3>
        <p>
          Once the AOI is defined the program extracts all the Landsat images
          that cover even part of the AOI during the time period selected by the
          user. Once the Landsat images are determined the program displays them
          in a pull down menu as highligted in the red box in Figure 8. The uses
          presses the arrow on the right hand of “Select a scene” and a list of
          available satellite images is displayed. The list of images contains
          the following information: 1) the first three characters of the name
          indicate the type of satellite image it represents (LE7 = Landsat 7
          and LC8 = Landsat 8); 2) the date the image was acquired; 3) the
          percent cloud cover for the image; and the row and path of the image.
          The user can scroll down the list and select one by clicking on the
          left mouse button.
        </p>
        <figure>
          <img src="static/er2_reset/help/image12.jpg" style={{ width: 600 }} />
          <figcaption>
            Figure 8: List of available satellite images that cover even part of
            the AOI during the user defined time period.
          </figcaption>
        </figure>

        <p>
          Once the user selects one of the Landsat images the thumbnail of the
          Landsat image is displayed below the selected image as shown in Figure
          9. The footprint of the satellite image and the extent of the clouds
          is displayed on the screen overlaid over the AOI. This allows the user
          to identify in more detail (beyond the thumbnail) if the satellite
          image is acceptable. In addition a table of the available weather
          stations that the model will use are displayed on a table at the
          bottom of the screen. The user can de-select any of them by clicking
          the blue check mark. Any weather stations that are de-selected will
          not be used by the model in run. Once the user is satisfied with the
          Landsat image and the weather station data they can select the RUN
          AGROET button and the model will start running.
        </p>
        <figure>
          <img
            src="static/er2_reset/help/image13.jpeg"
            style={{ width: 600 }}
          />
          <figcaption>
            Figure 9: Once the user selects a Landsat image its thumbnail is
            displayed as well as a table showing the weather stations the model
            will use and the data for each of them.
          </figcaption>
        </figure>

        <TokenNavLink
          className={tokenClasses}
          to={routes.INSTRUCTIONS_ADVANCED}
          token={this.props.token}
        >
          <span id={ids.INSTR_ADVANCED_LINK}>Advanced</span>
        </TokenNavLink>
      </div>
    );
  }
}

export default withRouter(Instructions);
