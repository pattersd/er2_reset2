// @flow
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useDropzone } from "react-dropzone";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import IconButton from "@material-ui/core/IconButton";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import OlStyle from "ol/style/Style";
import OlStyleCircle from "ol/style/Circle";
import OlStyleFill from "ol/style/Fill";
import OlStyleStroke from "ol/style/Stroke";
import OlStyleText from "ol/style/Text";
import * as olUtils from "@owsi/catena/er2_ui/util/ol";
import { nullFunc } from "@owsi/catena/er2_map_userlayers/js/utils";
import Map from "@owsi/catena/er2_map/src/ol_map";
import Theme from "../styles";
import { readFeatures, writeFeatures } from "../util/utils";
import type { SelectClimateType } from "../reducers/select_climate";

function title(str) {
  return str.replace(/(^|\s)\S/g, (t) => t.toUpperCase());
}

type PropsType = {
  catenaMap: Map,
  selectClimate: SelectClimateType,
  setHasHotAndColdAOIs: Function,
  setHasHotAndColdPoints: Function,
  setPoints: Function,
  theme: Theme,
  uploadResetClimate: Function,
};

const climateTypes = ["hot", "cold"];
const shapeTypes = ["pts", "aoi"];

const getStyle = (climateType, shapeType) => (feat) => {
  let shapeStyle;
  if (shapeType === "pts") {
    shapeStyle = {
      image: new OlStyleCircle({
        fill: new OlStyleFill({
          color: climateType === "hot" ? "red" : "blue",
        }),
        stroke: new OlStyleStroke({
          width: 1,
          color: "black",
        }),
        radius: 7,
      }),
    };
  } else if (shapeType === "aoi") {
    shapeStyle = {
      stroke: new OlStyleStroke({
        width: 2,
        color: climateType === "hot" ? "red" : "blue",
      }),
    };
  }
  return new OlStyle({
    ...shapeStyle,
    text: new OlStyleText({
      font: '20px "Roboto Condensed", sans-serif',
      fill: new OlStyleFill({
        color: "black",
        opacity: 1,
      }),
      offsetX: "0",
      offsetY: "20",
      padding: [2, 2, 2, 2],
      stroke: new OlStyleStroke({
        width: 2,
        color: "white",
        radius: 1,
      }),
      text: climateType === "hot" ? "Hot" : "Cold",
    }),
  });
};

function SelectClimate(props: PropsType) {
  const [climateVectors, setClimateVectors] = React.useState();
  const [drawInteraction, setDrawInteraction] = React.useState();

  function getLayerName(climateType, shapeType) {
    return `${climateType}_${shapeType}`;
  }

  function getVectorSource(climateType, shapeType) {
    const layerType = getLayerName(climateType, shapeType);
    return climateVectors[layerType].getSource();
  }

  function displayPoint(feat, climateType, shapeType) {
    feat.set(
      "POINTID",
      getVectorSource(climateType, shapeType).getFeatures().length + 1
    );
    getVectorSource(climateType, shapeType).addFeature(feat);
  }

  React.useEffect(() => {
    const vectorLayers = {};
    climateTypes.forEach((climateType) => {
      shapeTypes.forEach((shapeType) => {
        const layerType = getLayerName(climateType, shapeType);
        const vs = new VectorSource({
          projection: "EPSG:3857",
        });
        vectorLayers[layerType] = new VectorLayer({
          source: vs,
          style: getStyle(climateType, shapeType),
          zIndex: 2,
        });
        props.catenaMap.olmap.addLayer(vectorLayers[layerType]);
      });
    });
    setClimateVectors(vectorLayers);
    return () => {
      Object.keys(vectorLayers).forEach((layerName) => {
        props.catenaMap.olmap.removeLayer(vectorLayers[layerName]);
      });
    };
  }, []);

  React.useEffect(() => {
    if (climateVectors) {
      climateTypes.forEach((climateType) => {
        shapeTypes.forEach((shapeType) => {
          const layerType = getLayerName(climateType, shapeType);
          const vs = climateVectors[layerType].getSource();
          vs.clear();
          if (
            (shapeType === "pts" && props.selectClimate.hasHotAndColdPoints) ||
            (shapeType === "aoi" && props.selectClimate.hasHotAndColdAOIs)
          ) {
            if (
              props.selectClimate[layerType] &&
              props.selectClimate[layerType].features.length > 0
            ) {
              const feats = readFeatures(props.selectClimate[layerType]);
              vs.addFeatures(feats);
            }
          }
        });
      });
    }
  }, [props.selectClimate, climateVectors]);

  React.useEffect(() => {
    if (drawInteraction) {
      props.catenaMap.olmap.addInteraction(drawInteraction);
      return () => {
        props.catenaMap.olmap.removeInteraction(drawInteraction);
      };
    }
    return nullFunc;
  });

  const setMode = (e) => {
    if (e.target.value === "hotAndColdPoints") {
      props.setHasHotAndColdPoints(e.target.checked);
    } else if (e.target.value === "hotAndColdAOIs") {
      props.setHasHotAndColdAOIs(e.target.checked);
    }
  };

  const setPoint = (climateType, shapeType) => () => {
    let interaction;
    if (shapeType === "pts") {
      interaction = olUtils.makeDrawPointInteraction();
    } else {
      interaction = olUtils.makeDrawPolygonInteraction();
    }
    interaction.on("drawend", (evt) => {
      displayPoint(evt.feature, climateType, shapeType);
      setDrawInteraction(null);

      const featureCollection = JSON.parse(
        writeFeatures(getVectorSource(climateType, shapeType).getFeatures())
      );
      props.setPoints(climateType, shapeType, featureCollection);
    });
    setDrawInteraction(interaction);
  };

  const clearPoints = (climateType, shapeType) => () => {
    getVectorSource(climateType, shapeType).clear();
    const featureCollection = JSON.parse(writeFeatures([]));
    props.setPoints(climateType, shapeType, featureCollection);
  };

  function renderEditor(climateType, shapeType, visible) {
    const onDrop = (acceptedFiles, rejectedFiles) => {
      props.uploadResetClimate(
        climateType,
        shapeType,
        acceptedFiles,
        rejectedFiles
      );
    };
    const { getRootProps, getInputProps } = useDropzone({ onDrop });

    const style = {
      textAlign: "center",
    };
    const hasClimate =
      props.selectClimate[`${climateType}_${shapeType}`].features.length > 0;

    return (
      <div style={{ display: visible ? "block" : "none" }}>
        <div style={style}>
          <span style={{ width: 60, display: "inline-block" }}>
            {hasClimate && (
              <span
                style={{
                  color: "#267fca",
                  display: "inline-block",
                  fontSize: 18,
                  marginRight: 6,
                  textAlign: "left",
                }}
              >
                <FontAwesomeIcon icon={"check-square"} />
              </span>
            )}
            {title(climateType)}:
          </span>
          <IconButton
            className={props.theme.icon}
            onClick={setPoint(climateType, shapeType)}
            title={`Add ${title(climateType)}`}
            variant="contained"
          >
            <FontAwesomeIcon icon="pencil-alt" />
          </IconButton>
          <IconButton
            className={props.theme.icon}
            onClick={clearPoints(climateType, shapeType)}
            title={`Clear ${title(climateType)} ${shapeType}`}
            variant="contained"
          >
            <FontAwesomeIcon icon="trash" />
          </IconButton>
          <span {...getRootProps()}>
            <input {...getInputProps()} />
            <IconButton
              className={props.theme.icon}
              title={`Upload ${title(climateType)} ${shapeType}`}
              variant="contained"
            >
              <FontAwesomeIcon icon="upload" />
            </IconButton>
          </span>
        </div>
      </div>
    );
  }

  return (
    <div className={props.theme.controlBar}>
      <FormControlLabel
        control={
          <Checkbox
            checked={props.selectClimate.hasHotAndColdPoints}
            onChange={setMode}
            value="hotAndColdPoints"
            color="primary"
          />
        }
        label="Set Hot and/or Cold Points"
      />
      {renderEditor("cold", "pts", props.selectClimate.hasHotAndColdPoints)}
      {renderEditor("hot", "pts", props.selectClimate.hasHotAndColdPoints)}

      <FormControlLabel
        control={
          <Checkbox
            checked={props.selectClimate.hasHotAndColdAOIs}
            onChange={setMode}
            value="hotAndColdAOIs"
            color="primary"
          />
        }
        label="Set Hot and/or Cold AOIs"
      />
      {renderEditor("cold", "aoi", props.selectClimate.hasHotAndColdAOIs)}
      {renderEditor("hot", "aoi", props.selectClimate.hasHotAndColdAOIs)}
    </div>
  );
}

export default SelectClimate;
