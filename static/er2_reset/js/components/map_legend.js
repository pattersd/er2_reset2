// @flow
import React from 'react'
import { css } from 'aphrodite'
import { styles } from '../styles/root'
import type { FicusDataType } from './frameworks'
import type { SettingsBarType } from '../reducers/settings'

export type PropsType = {
    ficusData: FicusDataType,
    hasAnalysisPanel: boolean,
    hasTOC: boolean,
    placement: string,
    settings: SettingsBarType
}

function MapLegend(props: PropsType) {
    const hasFicusData = props.ficusData &&
        Object.keys(props.ficusData).length &&
        props.ficusData.mapfiles &&
        props.ficusData.mapfiles.length

    const legendPlacementStyles = {
        lowerLeft: {
            bottom: '30px',
            left: '10px',
        },
        lowerRight: {
            bottom: 0,
            right: '10px',
        },
        upperLeft: {
            top: '10px',
            left: '10px',
        },
        upperRight: {
            top: '120px',
            right: '10px',
        },
    }

    let server = window.location.origin
    // Check for django dev server, in which case mapserver requests just go to port 80
    if (window.location.port >= 8000) {
        server = `${window.location.protocol}//${window.location.hostname}`
    }

    let legendSrc
    let legendStyle = { display: 'none' }
    if (hasFicusData && props.placement !== 'none') {
        const md = props.ficusData.mapfiles[0]
        legendSrc = `${server}/cgi-bin/mapserv?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&LAYER=${md.layer}&Format=image/png&map=${md.mapfile}&foo=${Math.random()}`
        legendStyle = {
            position: 'absolute',
            backgroundColor: `rgba(255, 255, 255, ${props.settings.popupOpacity})`,
            ...legendPlacementStyles[props.placement],
        }
    }

    const classNames = [styles.mapCtr]
    if (!props.hasAnalysisPanel) classNames.push(styles.moveDown)
    if (!props.hasTOC) classNames.push(styles.moveLeft)

    return (
        <div className={css(classNames)} style={legendStyle}>
            <img src={legendSrc} alt="legend" />
        </div>
    )
}

export default MapLegend
