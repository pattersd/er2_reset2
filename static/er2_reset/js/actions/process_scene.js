// @flow
import {
    setMapError,
    onUpdateUserLayers,
} from "@owsi/catena/er2_map_userlayers/js/actions"
import { appendToLogger } from "@owsi/catena/er2_map_userlayers/js/actions/logger"
import { updateAppState } from "./server_state"
import * as types from "../constants/action_types"

export const resetRunning = (isRunning) => ({
    type: types.RESET_RUNNING,
    isRunning,
})

export const setResetOutputs = (layers) => ({
    type: types.SET_RESET_OUTPUTS,
    layers,
})
export const setResetProgress = (progress) => ({
    type: types.SET_RESET_PROGRESS,
    progress,
})
export const setRunOption = (option, checked) => ({
    type: types.SET_RUN_OPTION,
    option,
    checked,
})

const timeout = 10000
export const checkSceneProcess = (suid: string) => (
    dispatch: Function,
    getState: Function,
) => {
    let hasError
    const { scene } = getState().selectImage
    fetch(
        `/er2_reset/api/v1/process/?token=${
            getState().token.value
        }&suid=${suid}&sceneID=${scene.id}`,
    )
        .then((response) => {
            hasError = !response.ok
            if (hasError) {
                dispatch(resetRunning(false))
            }
            return response.json()
        })
        .then((ret) => {
            if (hasError) {
                dispatch(resetRunning(false))
                dispatch(appendToLogger([ret]))
                dispatch(setMapError(ret.exception.message))
            } else if (ret.state.suid) {
                setTimeout(() => {
                    dispatch(setResetProgress(ret.state.progress))
                    dispatch(checkSceneProcess(ret.state.suid))
                }, timeout)
            } else {
                dispatch(resetRunning(false))
                dispatch(appendToLogger(ret.state.log))
                if (ret.state.error) {
                    dispatch(setMapError(`Run failed: ${ret.state.error}`))
                }
                dispatch(onUpdateUserLayers())
                dispatch(setResetOutputs(ret.state.layers))
                dispatch(setResetProgress(ret.state.progress))
                dispatch(updateAppState())
            }
        })
        .catch((err) => {
            dispatch(resetRunning(false))
            dispatch(appendToLogger([err]))
            dispatch(setMapError(err.exception.message))
        })
}

export const onProcessScene = () => (
    dispatch: Function,
    getState: Function,
) => {
    dispatch(resetRunning(true))
    dispatch(setResetProgress())
    let hasError
    const { aoi } = getState().selectAOI
    const { scene } = getState().selectImage
    const { hasHotAndColdAOIs, hasHotAndColdPoints } = getState().selectClimate
    const { options } = getState().processScene
    const payload = {
        climate: getState().climate,
        aoi,
        hasHotAndColdAOIs,
        hasHotAndColdPoints,
        options,
        sceneID: scene.id,
    }
    const method = "POST"
    fetch(`/er2_reset/api/v1/process/?token=${getState().token.value}`, {
        method,
        body: JSON.stringify(payload),
    })
        .then((response) => {
            hasError = !response.ok
            return response.json()
        })
        .then((ret) => {
            if (hasError) {
                console.error(ret)
                dispatch(resetRunning(false))
                dispatch(appendToLogger([ret]))
                dispatch(setMapError(ret.exception.message))
            } else {
                dispatch(appendToLogger(["Scene submitted for processing"]))
                dispatch(onUpdateUserLayers())
                setTimeout(() => {
                    dispatch(checkSceneProcess(ret.state.suid))
                }, timeout)
            }
        })
        .catch((err) => {
            dispatch(resetRunning(false))
            dispatch(appendToLogger([err]))
            dispatch(setMapError(err.exception.message))
        })
}
