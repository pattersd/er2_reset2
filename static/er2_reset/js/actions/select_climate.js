// @flow
import request from "superagent";
import { appendToLogger } from "@owsi/catena/er2_map_userlayers/js/actions/logger";
import { onUpdateUserLayers } from "@owsi/catena/er2_map_userlayers/js/actions";
import * as types from "../constants/action_types";
import { updateAppState } from "./server_state";

export const updateResetClimate = (resetClimate) => ({
  type: types.UPDATE_RESET_CLIMATE,
  resetClimate,
});

export const uploadResetClimate = (climateType, shapeType, acceptedFiles) => (
  dispatch: Function,
  getState: Function
) => {
  const req = request.post(
    `/er2_reset/api/v1/climate/?token=${getState().token.value}`
  );
  acceptedFiles.forEach((file) => req.attach(file.name, file));
  req.field({ climate_type: climateType, shape_type: shapeType });
  req
    .then((resp) => {
      dispatch(updateResetClimate(resp.body.state));
      dispatch(onUpdateUserLayers());
      dispatch(updateAppState());
    })
    .catch((ret) => dispatch(appendToLogger([ret.response.body])));
};

export const setHasHotAndColdAOIs = (hasHotAndColdAOIs) => ({
  type: types.HAS_HOT_AND_COLD_AOIS,
  hasHotAndColdAOIs,
});
export const setHasHotAndColdPoints = (hasHotAndColdPoints) => ({
  type: types.HAS_HOT_AND_COLD_POINTS,
  hasHotAndColdPoints,
});
export const setPoints = (pointType, shapeType, featureCollection) => (
  dispatch: Function,
  getState: Function
) => {
  dispatch({
    type: types.SET_POINTS,
    pointType,
    shapeType,
    features: featureCollection,
  });

  // use patch to modify points and aois
  let hasError = false;
  fetch(`/er2_reset/api/v1/climate/?token=${getState().token.value}`, {
    method: "PATCH",
    body: JSON.stringify({
      point_type: pointType,
      shape_type: shapeType,
      feature_collection: featureCollection,
    }),
  })
    .then((response) => {
      hasError = !response.ok;
      return response.json();
    })
    .then((ret) => {
      if (hasError) {
        console.error(ret);
        dispatch(appendToLogger(ret));
      } else {
        dispatch(updateResetClimate(ret.state));
        dispatch(updateAppState());
        dispatch(onUpdateUserLayers());
        dispatch(appendToLogger(["Updated point"]));
      }
    });
};
