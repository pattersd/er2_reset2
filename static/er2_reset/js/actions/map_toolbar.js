import * as types from '../constants/action_types'

// @flow
export const onClickMapToolbarItem = (id: string) => ({ type: types.CLICK_MAP_TOOLBAR_ITEM, id })