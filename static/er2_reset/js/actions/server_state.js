// @flow
import Cookies from "universal-cookie"
import {
    onFetchMapState,
    setTabbedPanelWindowState,
} from "@owsi/catena/er2_map_userlayers/js/actions"
import { updateUser } from "@owsi/catena/er2_ui"

import { windowStates } from "@owsi/catena/er2_map_userlayers/js/components/window_state"
import * as types from "../constants/action_types"

const setFetchingState = (isFetching: boolean) => ({
    type: types.SET_FETCHING_STATE,
    isFetching,
})

const setFetchedState = (
    isFetched: boolean,
    state: { token: { value: string } },
    meta: { version: string },
) => ({ type: types.SET_FETCHED_STATE, isFetched, state, meta })

const updateMeta = (name: string, version: string, user: {}) => ({
    type: types.UPDATE_META,
    name,
    version,
    user,
})

const getFetchPost = () => {
    const cookies = new Cookies()
    const csrftoken = cookies.get("csrftoken")
    return {
        credentials: "include",
        method: "POST",
        mode: "same-origin",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            "X-CSRFToken": csrftoken,
        },
        body: {},
    }
}

export const fetchState = () => (dispatch, getState) => {
    let fetchError = false
    dispatch(setFetchingState(true))
    fetch(`/${getState().meta.name}/api/v1/state/`)
        .then((response) => {
            fetchError = !response.ok
            return response.json()
        })
        .then((json) => {
            dispatch(setFetchingState(false))
            if (fetchError) {
                dispatch(setFetchedState(true, {}))
            } else {
                dispatch(setFetchedState(true, json.state, json.meta))
                dispatch(
                    updateMeta(
                        json.meta.name,
                        json.meta.version,
                        json.meta.user,
                    ),
                )
                dispatch(updateUser(json.meta.user))
                dispatch(onFetchMapState())
                dispatch(setTabbedPanelWindowState(windowStates.opened))
            }
        })
}

export const updateAppState = (appStateType) => (dispatch, getState) => {
    const state = getState()
    const appState = appStateType
        ? {
              [appStateType]: state[appStateType],
          }
        : {
              climate: state.climate,
              processScene: state.processScene,
              selectAOI: state.selectAOI,
              selectClimate: state.selectClimate,
              selectImage: state.selectImage,
          }

    const options = getFetchPost()
    options.method = "PATCH"
    options.body = JSON.stringify({ appState })
    fetch(`/${getState().meta.name}/api/v1/state/`, options)
}

export const onLogout = () => (dispatch, getState) => {
    const fetchPost = getFetchPost()
    fetch("/login/logout/", fetchPost).then(() => {
        window.location = "/"
    })
}

const fetchWithCatenaError = (url, args, dispatch, success, method) => {
    let fetchError = false
    fetch(url, {
        method: method || "POST",
        credentials: "include",
        headers: {
            "Content-Type": "application/json",
            "X-CSRFToken": Cookies.get("csrftoken"),
            "X-Requested-With": "XMLHttpRequest",
        },
        body: JSON.stringify(args),
    })
        .then((response) => {
            fetchError = !response.ok
            return response.json()
        })
        .then((json) => {
            success(json)
        })
}

export const onRemoveUser = (user) => (dispatch, getState) => {
    fetchWithCatenaError("/login/api/v1/remove/", user, dispatch, (json) => {
        window.location = "/login/"
    })
}

export default fetchState
