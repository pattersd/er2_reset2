// @flow
import request from "superagent"
import {
    appendToLogger,
    onUpdateUserLayers,
    setMapError,
} from "@owsi/catena/er2_map_userlayers/js/actions"
import * as types from "../constants/action_types"
import { updateClimate } from "./climate"
import { updateResetClimate } from "./select_climate"
import { updateAppState } from "./server_state"
import { maybeFindImagery, resetSearch, setSearchFeature } from "./select_image"
import type { ImageryType } from "../reducers/select_image"

export const aoiRunning = (isRunning) => ({
    type: types.AOI_RUNNING,
    isRunning,
})
export const setAOI = (aoi: {}) => ({ type: types.SET_AOI, aoi })

export const generateInputs = (aoi: {}, scene: ImageryType) => (
    dispatch: Function,
    getState: Function,
) => {
    dispatch(aoiRunning(true))
    const { startDate, endDate } = getState().selectImage
    let hasError
    fetch(
        `/er2_reset/api/v1/generate_inputs/?token=${getState().token.value}`,
        {
            method: "POST",
            body: JSON.stringify({
                aoi,
                sceneID: scene.id,
                startDate,
                endDate,
            }),
        },
    )
        .then((response) => {
            hasError = !response.ok
            return response.json()
        })
        .then((ret) => {
            if (hasError) {
                console.error(ret)
                dispatch(setMapError(ret.exception.message))
            } else {
                dispatch(updateClimate(ret.state.climate))
                dispatch(onUpdateUserLayers())
                dispatch(updateAppState())
                // Refresh cold and hot point features
                dispatch(updateResetClimate(ret.state.reset_climate))
            }
            dispatch(aoiRunning(false))
        })
        .catch((ret) => {
            dispatch(appendToLogger([ret.response.body]))
            dispatch(aoiRunning(false))
        })
}

export const uploadAOI = (acceptedFiles) => (
    dispatch: Function,
    getState: Function,
) => {
    dispatch(resetSearch())
    const req = request.post(
        `/er2_map/api/v1/get_geojson/?token=${getState().token.value}`,
    )
    acceptedFiles.forEach((file) => req.attach(file.name, file))
    req.then((resp) => {
        const aoi = resp.body.geojson
        if (aoi) {
            dispatch(setAOI(aoi))
            dispatch(updateAppState())
            dispatch(setSearchFeature(aoi))
            dispatch(maybeFindImagery())
        } else {
            dispatch(
                setMapError(
                    "Failed to upload AOI. Be sure that you have selected all components of the shapefile.",
                ),
            )
        }
    }).catch((ret) => dispatch(appendToLogger([ret.response.body])))
}
