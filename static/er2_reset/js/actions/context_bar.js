import * as types from '../constants/action_types'

// @flow
export const onClickContextBarItem = (id: string, active: boolean) =>
    ({ type: types.CLICK_CONTEXT_BAR_ITEM, id, active })