import * as types from '../constants/action_types'

// @flow
export const onPatchState = (isFetching: boolean) =>
    ({ type: types.PATCH_STATE, isFetching })

// @flow
export const onReceiveState = (isFetching: boolean) =>
    ({ type: types.RECEIVE_STATE, isFetching })

// @flow
export const onReceiveStateError = (json: object) =>
    ({ type: types.RECEIVE_STATE_ERROR, json })