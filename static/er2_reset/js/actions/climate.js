// @flow
import { updateAppState } from './server_state'
import * as types from '../constants/action_types'

export const onSetStationChecked = (id: number, checked: boolean) => (dispatch: Function, getState: Function) => {
    dispatch({ type: types.SET_STATION_CHECKED, id, checked })
    dispatch(updateAppState('climate'))
}
export const updateClimate = climate => ({ type: types.UPDATE_CLIMATE_STATIONS, climate })
