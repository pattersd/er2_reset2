// @flow
import {
    onUpdateUserLayers,
    onUserLayersPropsChange,
    setMapError,
} from "@owsi/catena/er2_map_userlayers/js/actions/map"
import { appendToLogger } from "@owsi/catena/er2_map_userlayers/js/actions/logger"
import * as types from "../constants/action_types"
import type { ImageryType } from "../reducers/select_image"
import type { GeoJSONType } from "../reducers/climate"
import { updateAppState } from "./server_state"

export const setCurrentScene = (scene: ImageryType) => ({
    type: types.SET_CURRENT_SCENE,
    scene,
})
export const setSearchFeature = (searchFeature: GeoJSONType) => ({
    type: types.SET_SEARCH_FEATURE,
    searchFeature,
})
export const setSearchDate = (dateType, date) => ({
    type: types.SET_SEARCH_DATE,
    dateType,
    date,
})
export const setSearchPath = (path) => ({ type: types.SET_PATH, path })
export const setSearchRow = (row) => ({ type: types.SET_ROW, row })
export const searchRunning = (isRunning) => ({
    type: types.SEARCH_RUNNING,
    isRunning,
})
export const updateImagery = (results) => ({
    type: types.UPDATE_IMAGERY,
    results,
})
export const resetSearch = () => ({ type: types.RESET_SEARCH })

export const hideBQA = () => (dispatch, getState) => {
    const mapsources = getState().mapsources
    mapsources.mapsources.forEach((m) => {
        m.layers.forEach((l) => {
            if (l.visible && l.name.indexOf("BQA") >= 0) {
                dispatch(
                    onUserLayersPropsChange(l.mapsource, l, { visible: false }),
                )
            }
        })
    })
}

export const onFindImagery = () => (dispatch: Function, getState: Function) => {
    dispatch(searchRunning(true))
    let hasError
    const { searchFeature, startDate, endDate } = getState().selectImage
    const { selectionType } = getState().settings
    fetch(`/er2_reset/imagery/?token=${getState().token.value}`, {
        method: "POST",
        body: JSON.stringify({
            aoi: searchFeature,
            start_date: startDate,
            end_date: endDate,
            use_satsearch: selectionType === "satsearch",
        }),
    })
        .then((ret) => {
            hasError = !ret.ok
            return ret.json()
        })
        .then((json) => {
            if (hasError) {
                dispatch(setMapError(json.exception.message))
                dispatch(appendToLogger([json]))
            } else {
                dispatch(updateImagery(json.results))
                dispatch(updateAppState())
            }
            dispatch(searchRunning(false))
        })
}

export const onFindImageryByRowAndPath = () => (
    dispatch: Function,
    getState: Function,
) => {
    dispatch(searchRunning(true))
    const { row, path, startDate, endDate } = getState().selectImage
    const { selectionType } = getState().settings
    fetch(`/er2_reset/imagery_row_path/?token=${getState().token.value}`, {
        method: "POST",
        body: JSON.stringify({
            path,
            row,
            start_date: startDate,
            end_date: endDate,
            use_satsearch: selectionType === "satsearch",
        }),
    })
        .then((ret) => {
            return ret.json()
        })
        .then((json) => {
            dispatch(updateImagery(json.results))
            dispatch(searchRunning(false))
        })
}

export const onSetScene = (scene: ImageryType) => (
    dispatch: Function,
    getState: Function,
) => {
    dispatch(searchRunning(true))
    const { searchFeature, startDate, endDate } = getState().selectImage
    let hasError
    fetch(`/er2_reset/api/v1/scene/?token=${getState().token.value}`, {
        method: "POST",
        body: JSON.stringify({
            scene,
            aoi: searchFeature,
            start_date: startDate,
            end_date: endDate,
        }),
    })
        .then((response) => {
            dispatch(searchRunning(false))
            hasError = !response.ok
            return response.json()
        })
        .then((ret) => {
            if (hasError) {
                console.error(ret)
                dispatch(appendToLogger([ret]))
            } else {
                dispatch(setCurrentScene(scene))
                dispatch(onUpdateUserLayers())
                dispatch(updateAppState())
            }
        })
        .catch((r) => {
            dispatch(setMapError(r.exception.message))
        })
}

export const maybeFindImagery = () => (dispatch, getState) => {
    const { searchFeature, startDate, endDate } = getState().selectImage
    if (searchFeature && startDate && endDate) {
        dispatch(onFindImagery())
    } else {
        dispatch(updateImagery([]))
        dispatch(setCurrentScene(null))
        dispatch(hideBQA())
    }
}
