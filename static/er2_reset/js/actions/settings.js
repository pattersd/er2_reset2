// @flow
import * as types from '../constants/action_types'

export const updateSettings = settings => ({ type: types.UPDATE_SETTINGS, settings })
