// @flow
import * as types from '../constants/action_types'
import { ViewType } from '../reducers/map'

export const updateCurrentView = (view: ViewType) => (dispatch: Function, getState: Function) => {
    dispatch({ type: types.UPDATE_MAP_VIEW, mapView: view })
}
