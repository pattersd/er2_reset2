// Styles for the application root

import baseStyles from "@owsi/catena/er2_styles";

const parentStyles = {
  ...baseStyles,
  root: {
    ...baseStyles.root,
    gridTemplateColumns: `[left] 50px [controlBar] 150px [tocBar] 175px [main] 18px [breadcrumbOverflow]
            3fr [toolbarPanel] 255px [wideToolbar] 100px [toolbar] 50px [right]`,
  },
  analysisPanel: {
    ...baseStyles.analysisPanel,
    gridColumnEnd: "toolbarPanel",
  },
  AOIcontainer: {
    borderRadius: 6,
    marginTop: 10,
  },
  cell: {
    textAlign: "center",
  },
  controlBar: {
    ...baseStyles.controlBar,
    boxShadow: "initial",
  },
  dateEntry: {
    width: 100,
  },
  icon: {
    color: "#267fca",
  },
  checkbox: {
    height: "init",
    padding: 0,
  },
  label: {
    fontSize: 14,
    marginLeft: 10,
  },
  listContainer: {
    display: "inline-flex",
    height: 20,
    maxHeight: 20,
    padding: 0,
    margin: 0,
  },
  progressContainer: {
    border: "1px solid gray",
    display: "flex",
    flexDirection: "column-reverse",
    fontSize: "smaller",
    margin: "20px 6px",
    maxHeight: 100,
    overflow: "auto",
    padding: 6,
    textAlign: "left",
  },
  stupidBackgroundColorOrange: {
    backgroundColor: "orange",
  },
  stupidBackgroundColorYellow: {
    backgroundColor: "yellow",
  },
};

export default parentStyles;
