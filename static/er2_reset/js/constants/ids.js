/**
 * Contains ids assigned to elements in the DOM.  Useful to store these
 * here because the ids are used in actions and reducers to determine
 * the source of redux actions and ui interactions.
 */
export const DOCUMENTATION_LINK = 'documentationLink'
export const INSTR_PURPOSE_LINK = 'purpose'
export const INSTR_GETTING_STARTED_LINK = 'getting-started'
export const INSTR_ADVANCED_LINK = 'advanced'
export const PROCESS_OPTIONS = 'processOptions'
export const PROCESS_SCENE_SUBHEADER = 'processScene'
export const SELECT_AOI = 'selectAOI'
export const SELECT_AOI_SUBHEADER = 'selectAOI'
export const SELECT_CLIMATE = 'selectClimate'
export const SELECT_CLIMATE_SUBHEADER = 'selectClimate'
export const SELECT_IMAGE = 'selectImage'
export const SELECT_IMAGE_SUBHEADER = 'selectImage'
export const PROCESS_SCENE = 'processScene'
export const SETTINGS = 'settings'
export const SUBHEADER_DOCS = 'subheaderDocs'
