// These date formats are consistent with moment.js conventions:
// https://momentjs.com/docs/#/parsing/string-format/
export const WEB_DISPLAY = 'MM-DD-YYYY'
export const ISO_DATE = 'YYYY-MM-DD'