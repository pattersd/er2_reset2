// Application url routes are contained here.
export const HOME = "/"
export const INSTRUCTIONS = "/docs"
export const INSTRUCTIONS_ADVANCED = "/docs/advanced"
export const INSTRUCTIONS_GETTING_STARTED = "/docs/getting-started"
export const INSTRUCTIONS_PURPOSE = "/docs/purpose"
export const SELECT_CLIMATE = "/select-climate/"
export const SELECT_AOI = "/selectAOI/"
export const SELECT_IMAGE = "/selectImage/"
export const PROCESS_OPTIONS = "/processOptions/"
export const PROCESS_SCENE = "/processScene/"
export const SETTINGS = "/settings/"
export const USER = "/user/"
