/* eslint-disable */
const webpack = require("webpack");
const debug = process.env.NODE_ENV !== "production";
var path = require("path");

// Writes the CSS code processed by Webpack into its own CSS-file and not into the generated bundle JavaScript file.
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  context: __dirname + "/er2_reset",
  entry: "./js/index.js",
  output: {
    path: __dirname + "/er2_reset/dist",
    filename: "index.min.js",
  },
  module: {
    rules: [
      {
        // Compile all Javascript using Babel (with React and Flow), targeting the 2 most recent browsers
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: (resourcePath, context) =>
                // publicPath is the relative path of the resourc2e to the context
                // e.g. for ./css/admin/main.css the publicPath will be ../../
                // while for ./css/main.css the publicPath will be ../
                `${path.relative(path.dirname(resourcePath), context)}/`,
            },
          },
          "css-loader",
        ],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ["file-loader"],
      },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader",
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "[name].css",
      chunkFilename: "[id].css",
    }),
  ],
  resolve: {
    alias: {
      path: "path-browserify",
    },
  },
};
