module.exports = function (api) {
    api.cache(true)

    const presets = [
        '@babel/env',
        '@babel/react',
        '@babel/flow',
    ]
    const plugins = [
        '@babel/plugin-proposal-export-default-from',
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-syntax-export-namespace-from',
    ]

    return {
        presets,
        plugins,
    }
}
