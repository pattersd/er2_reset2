import django.shortcuts
from django.views.decorators.csrf import csrf_exempt
import er2.common.api as er2_api
from er2.apps.er2_reset import services as svcs


def return_with_exception(view_fn):
    def wrapper(*args, **kwargs):
        try:
            return view_fn(*args, **kwargs)
        except Exception as e:
            return er2_api.respond_with_exception(e)

    return wrapper


def get_token(request):
    token = request.session.get("token", None)
    if token:
        return {"value": token}


@csrf_exempt
def index(request):
    token = None
    if request.user.is_authenticated:
        token = request.session.get("token", None)

    if not token:
        # Log in.
        response = django.shortcuts.redirect("login_view")
        if request.META["QUERY_STRING"]:
            get_args_str = request.META["QUERY_STRING"]
            response["Location"] += "?" + get_args_str
        return response

    ctx = {"version": svcs.get_version()}
    return django.shortcuts.render(request, "er2_reset/index.html", context=ctx)


@csrf_exempt
@return_with_exception
def state(request):
    """Returns the state of an application."""
    if request.user.is_authenticated:
        return svcs.State(request=request, token=get_token(request)).process()
    raise Exception("State failed")


@csrf_exempt
@return_with_exception
def find_imagery(request):
    """Find imagery for the given extent"""
    return svcs.ImageryState(request=request, token=get_token(request)).process()


@csrf_exempt
@return_with_exception
def find_imagery_row_path(request):
    """Find imagery for the given extent"""
    return svcs.ImageryState(
        request=request, token=get_token(request)
    ).get_imagery_products_row_path()


@csrf_exempt
@return_with_exception
def set_scene(request):
    s = svcs.InputState(request=request, token=get_token(request))
    deleted = s.cleanup()
    return svcs.SceneState(request=request, token=get_token(request)).process()


@csrf_exempt
@return_with_exception
def climate(request):
    """Update climate station info for the given extent"""
    return svcs.ClimateState(request=request, token=get_token(request)).process()


@csrf_exempt
@return_with_exception
def generate_inputs(request):
    return svcs.InputState(request=request, token=get_token(request)).process()


@csrf_exempt
@return_with_exception
def process_scene(request):
    return svcs.ProcessSceneState(request=request, token=get_token(request)).process()


@csrf_exempt
@return_with_exception
def download_climate(request):
    return svcs.InputState(request=request, token=get_token(request)).download_climate()
