#!/usr/bin/env bash

service apache2 start
# App should be mounted?
cp /tmp/fonts.txt /app
python3 /app/manage.py runserver 0.0.0.0:8000
tail -f /dev/null
