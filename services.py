import binascii
import csip.utils
import datetime
from django.template.loader import render_to_string
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import smtplib
import fiona
import geopandas
import glob
import hashlib
import json
import logging
import os
import pymongo
import pytz
import requests
import subprocess
import shutil
import ssl
import tempfile
from shapely.wkt import dumps, loads
from shapely.geometry import shape
import time
import er2.common.api as er2_api
import er2.common.config as er2_config
import er2.common.state as er2_state
import er2.common.strings as er2_strings
import er2.apps.er2_map.gis.view as er2_view
from er2.apps.er2_map import services as svcs_map
from er2.apps.er2_map.savefile import jsondump
import er2.apps.er2_map.gis.layer.layer_factory as lf
from er2.apps.er2_reset.climate.main import Climate
from er2.apps.er2_reset.imagery.main import Imagery


users_path = "/gis_data/users/users_agroet_hashed.json"


def get_cwd():
    """Return the current working directory of this app"""
    return os.path.abspath(os.path.dirname(os.path.abspath(__file__)))


def load_config():
    """Load the application configuration.

    Returns:
        dict: application config as defined in config.yml
    """
    return er2_config.load_config(os.path.join(get_cwd(), "config.yml"))


def get_version():
    """Returns the current version of the application"""
    return load_config().get("version", "")


def get_app_name():
    """Returns the name of the application"""
    return load_config().get("name", "")


def get_api_resources():
    """Return a list of all API resources associated with the app.

    Used by eRAMS to populate a self-describing API at /api

    Revi turns:
        A list of api resources
    """
    import er2.common.urls as er2_urls
    import er2.apps.er2_reset.urls as app_urls

    api_urls = []
    for url, prefix in er2_urls.walk(app_urls.urlpatterns, get_app_name()):
        if "api" in url.regex.pattern.split("/"):
            meta = {
                "selfLink": er2_urls.prettify(
                    "/" + "/".join([prefix, url.regex.pattern])
                )
            }
            api_urls.append(er2_api.ApiResource(version=get_version(), meta=meta))
    return [dict(url) for url in api_urls]


def er2_login_enable_registration(default=None):
    return True


def er2_login_disable_oauth(default=None):
    return True


def er2_login_enable_direct_sign_in(default=None):
    return True


def er2_login_get_page_name(default=None):
    return "AgroET"


def get_user_collection():
    """Returns a mongodb connection from the connection pool"""
    c = pymongo.MongoClient(
        os.getenv("MONGODB_HOST", "mongodb"), int(os.getenv("MONGODB_PORT", 27017))
    )
    return c.agroet.users


def get_projects_collection():
    """Returns a mongodb connection from the connection pool"""
    c = pymongo.MongoClient(
        os.getenv("MONGODB_HOST", "mongodb"), int(os.getenv("MONGODB_PORT", 27017))
    )
    return c.agroet.projects


def hash_password(password):
    """Hash a password for storing."""
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode("ascii")
    pwdhash = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode("ascii")


def verify_password(stored_password, provided_password):
    """Verify a stored password against one provided by user"""
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac(
        "sha512", provided_password.encode("utf-8"), salt.encode("ascii"), 100000
    )
    pwdhash = binascii.hexlify(pwdhash).decode("ascii")
    return pwdhash == stored_password


def add_or_update_user(user_attrs):
    username = user_attrs.get("id", user_attrs.get("username"))

    # Check if the user exists
    users_coll = get_user_collection()
    user_doc = users_coll.find_one({"_id": username}) or {}
    user_doc["_id"] = username
    user_doc.update(user_attrs)
    if "password" in user_attrs:
        user_doc["password"] = hash_password(user_attrs["password"])

    logging.getLogger("er2").info(user_doc)
    users_coll.replace_one({"_id": username}, user_doc, upsert=True)
    user_doc = get_user_doc(username)
    return user_doc


def get_user(username):
    doc = get_user_doc(username)
    del doc["password"]
    return doc


def get_user_doc(username):
    users_coll = get_user_collection()
    return users_coll.find_one({"_id": username})


def get_user_doc_from_registration_token(code):
    users_coll = get_user_collection()
    return users_coll.find_one({"registration_token": code})


def authenticate_user(username, password):
    # Check if the user exists
    user_doc = get_user_doc(username)
    if user_doc is not None:
        if verify_password(user_doc["password"], password):
            return
        return "Password failed"
    return "User does not exist"


def er2_login_handle_authentication(request, user_attrs):
    user_id, email, first_name, last_name, attrs = [None] * 5
    user_id = user_attrs["username"]

    # load the user dict
    user_info = get_user_doc(user_id)
    if user_info:
        pw_hashed = user_info["password"]
        if "registration_token" in user_info:
            attrs = {
                "error": "Email address not verified",
            }
        elif verify_password(pw_hashed, user_attrs["password"]):
            defaultContext = "/selectImage/"
            url = user_attrs.get("next", defaultContext) or defaultContext
            email = user_info["email"]
            first_name = user_info["firstName"]
            last_name = user_info["lastName"]
            attrs = {
                "projects": [
                    {
                        "name": user_id,
                        "type": "AgroET",
                        "url": url,
                    },
                ],
                "project_token": user_info["project"],
            }
        else:
            attrs = {
                "error": "Login failed",
            }
    else:
        attrs = {
            "error": "User does not exist",
        }
    return user_id, email, first_name, last_name, attrs


def er2_login_handle_registration(request, source, user_attrs):
    user_id = user_attrs["username"]

    # Check if the user exists
    user_doc = get_user_doc(user_id)
    if user_doc:
        raise Exception("Username {0} already exists".format(user_id))

    pw = user_attrs.get("password", None)
    if not pw:
        raise Exception("No password provided")

    # create a project for the user
    user_attrs["project"] = er2_strings.rand()
    user_attrs["registration_token"] = er2_strings.rand()

    send_confirmation(request, user_attrs)

    add_or_update_user(user_attrs)

    attrs = {
        "projects": [
            {
                "name": user_id,
                "type": "AgroET",
                "url": "/docs/",
            },
        ],
        "project_token": user_attrs["project"],
    }

    return (
        user_id,
        user_attrs["email"],
        user_attrs["firstName"],
        user_attrs["lastName"],
        attrs,
    )


def er2_login_handle_registration_update(request, source, user_attrs):
    user_doc = get_user_doc(user_attrs["id"])
    if not user_doc:
        raise Exception("User {0} does not exist".format(user_attrs["id"]))

    # Initialize new fields
    for k in ["email", "firstName", "lastName"]:
        if not k in user_doc:
            user_doc[k] = ""
    user_doc.update(user_attrs)

    # Check for new password
    if "password" in user_doc:
        pw = user_doc["password"]
        pw_retyped = user_doc.pop("passwordRetyped")
        if pw != pw_retyped:
            raise Exception("Passwords do not match")
    add_or_update_user(user_doc)
    return (
        user_attrs["id"],
        user_doc["email"],
        user_doc["firstName"],
        user_doc["lastName"],
        user_doc.get("attrs", {}),
    )


def er2_login_handle_email_confirmation(request, code):
    user_doc = get_user_doc_from_registration_token(code)
    if user_doc:
        del user_doc["registration_token"]
        users_coll = get_user_collection()
        users_coll.replace_one({"_id": user_doc["_id"]}, user_doc, upsert=True)
        return True
    return False


def er2_login_handle_removal(request, user):
    user_id = user.id

    # Check if the user exists
    user_doc = get_user_doc(user_id)
    if not user_doc:
        raise Exception("User {0} does not exist".format(user_id))

    users_coll = get_user_collection()
    user_doc = users_coll.find_one({"_id": user_id})
    if user_doc:
        users_coll.delete_one({"_id": user_id})
    else:
        raise er2_api.ApiException(f"User {user_id} does not exist")
    return True


def send_confirmation_busted(user_attrs):
    port = 587  # For starttls
    smtp_server = "smtp.gmail.com"

    smtp_server = "smtp.gmail.com"
    sender_email = "agroET@irrigator.co"
    receiver_email = user_attrs["email"]
    password = os.environ.get("EMAIL_PASSWORD", None)

    html_part = render_to_string(
        "er2_reset/confirm_email.html",
        {"code": user_attrs["registration_token"]},
    )

    context = ssl._create_unverified_context()

    msg = MIMEMultipart("related")
    msg["From"] = sender_email
    msg["To"] = receiver_email
    msgAlternative = MIMEMultipart("alternative")
    msg.attach(msgAlternative)

    msgText = MIMEText("This is the alternative plain text message.")
    msgAlternative.attach(msgText)
    msgAlternative.attach(html_part)

    with smtplib.SMTP(smtp_server, port) as server:
        server.starttls(context=context)
        server.login("resetagro@gmail.com", password)
        server.sendmail(sender_email, [receiver_email], msg)


def send_confirmation(request, user_attrs):
    port = 587  # For starttls
    smtp_server = "smtp.gmail.com"
    sender_email = "resetagro@gmail.com"
    receiver_email = user_attrs["email"]
    password = os.environ.get("EMAIL_PASSWORD", None)

    context = ssl._create_unverified_context()
    body = f"""
Hello {user_attrs['firstName']},

Please go to http://{request.get_host()}/login/confirm/?code={user_attrs['registration_token']} to confirm your email address.
    """
    with smtplib.SMTP(smtp_server, port) as server:
        server.starttls(context=context)
        server.login(sender_email, password)
        msg = f"""Subject: Confirm Your AgroET Email
From: agroET@irrigator.co
To: {receiver_email}

{body}
"""
        server.sendmail(sender_email, [receiver_email], msg)


class State(er2_state.State):
    def __init__(
        self, props=None, request=None, version=get_version(), meta=None, token=None
    ):
        super().__init__(
            props=props,
            request=request,
            version=version,
            meta=meta,
            token=token,
            project="agroet",
        )

    def get_app_state_path(self):
        return os.path.join("/gis_data", self._token["value"], "appStateAgroET.json")

    def get_state(self):
        state = super().get_state()
        state = self.update_state(state)
        return state

    def update_state(self, state):
        if "appState" not in state:
            state["appState"] = {}
            app_state_path = self.get_app_state_path()
            if os.path.exists(app_state_path):
                with open(app_state_path) as fp:
                    try:
                        state["appState"] = json.load(fp)
                    except json.JSONDecodeError:
                        logging.getLogger("er2").error(
                            f"Unable to read stormwater app state file {app_state_path}"
                        )
        return state

    def logout(self):
        # Dump state to user dir
        state = self.get_state()
        app_state_path = self.get_app_state_path()
        jsondump(app_state_path, state["appState"])
        return er2_api.respond({})

    def PATCH(self, *args, **kwargs):
        new_state = er2_api.load_json(self._request.body)
        self.state = self.merge_state(new_state)
        self.set_state(self.state)
        self.logout()
        return er2_api.respond(self)


class ResetStateBase(er2_state.State):
    """ReSET common functions"""

    imagery_cache_dir = "/reset"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._mapSvc = svcs_map.MapState(request=self._request, token=self._token)
        self._mapsource = self._mapSvc.get_mapsource()

    def get_scene(self, scene, use_satsearch=False):
        imagery = Imagery()
        # Be sure the layer is downloaded
        bqa = imagery.get_band(scene, "BQA")
        bqa_path = imagery.get_band_path(scene, "BQA")

        # Set all other layers invisible
        for layer in self._mapsource.layers:
            if "BQA" not in layer.name:
                layer.set_properties({"visible": False})
        file_name = os.path.basename(bqa_path)
        link_path = os.path.join(self._mapsource.path, file_name)
        if os.path.islink(link_path):
            os.unlink(link_path)
        os.symlink(bqa_path, link_path)
        bqa_layer = lf.layer_factory(link_path)
        bqa_layer.classify(er2_view.CLASSIFY_UNIQUE_VALUES, nclass=50)
        classifications = bqa_layer.view_list["default"].classifications
        # Set 2720 to white
        del classifications[1]

        # Set border to light gray
        list(filter(lambda x: x.label == 1, classifications))[0].color = "#808080"
        bqa_layer.set_properties({"opacity": 90, "visible": True, "is_reset": True})
        self._mapsource.add_layer(bqa_layer)
        self._mapSvc.update_and_write_mapfile()

        self.set_state(self.state)
        return bqa_layer

    def get_reset_climate(self):
        """
        Return <cold/hot>_pts.shp and <cold/hot>_aoi.shp
        :return:
        """
        ret = {}
        for point_type in ["cold", "hot"]:
            ret[point_type] = {}
            for ver in ["pts", "aoi"]:
                fname = "user_{0}_{1}.shp".format(point_type, ver)
                path = os.path.join(self._mapsource.path, fname)
                if os.path.exists(path):
                    layer = lf.layer_factory(path)
                    ret[point_type][ver] = layer.get_store().as_geojson(crs="EPSG:4326")
                else:
                    ret[point_type][ver] = {
                        "type": "FeatureCollection",
                        "features": [],
                    }
        return ret

    def set_reset_climate(self, point_type, ver, feature_collection):
        fname = "user_{0}_{1}.shp".format(point_type, ver)
        path = os.path.join(self._mapsource.path, fname)
        if len(feature_collection["features"]):
            layer_type = feature_collection["features"][0]["geometry"]["type"]
            schema = {
                "geometry": layer_type,
                "properties": {"POINTID": "int", "GRID_CODE": "float"},
            }
            with fiona.open(
                path, "w", "ESRI Shapefile", schema, crs={"init": "epsg:4326"}
            ) as c:
                for f in feature_collection["features"]:
                    if "GRID_CODE" not in f["properties"]:
                        f["properties"]["GRID_CODE"] = 0
                    c.write(f)
        else:
            # Just delete the file.
            if os.path.exists(path):
                l = lf.layer_factory(path)
                l.delete()


class ImageryState(ResetStateBase):
    def get_imagery_pr2oducts_row_path(self):
        body = json.loads(self._request.body.decode("utf-8"))
        imagery = Imagery()
        start_date = datetime.datetime.strptime(body["start_date"][:10], "%Y-%m-%d")
        end_date = datetime.datetime.strptime(body["end_date"][:10], "%Y-%m-%d")
        ret = imagery.get_products_row_path(
            body["row"], body["path"], start_date, end_date
        )
        return er2_api.respond({"results": ret})

    def POST(self, *args, **kwargs):
        # For now assume that the user is starting a new project.
        body = json.loads(self._request.body.decode("utf-8"))
        imagery = Imagery()
        start_date = datetime.datetime.strptime(body["start_date"][:10], "%Y-%m-%d")
        end_date = datetime.datetime.strptime(body["end_date"][:10], "%Y-%m-%d")

        # Make sure properties are set or geopandas will barf.
        for f in body["aoi"]["features"]:
            f["properties"] = {}
        df = geopandas.GeoDataFrame.from_features(body["aoi"]["features"])
        # Make sure properties are set or geopandas will barf.
        for f in body["aoi"]["features"]:
            f["properties"] = {}
        df = geopandas.GeoDataFrame.from_features(body["aoi"]["features"])
        results = []
        centroid = df.unary_union.centroid
        centroid = [centroid.x, centroid.y]
        results += imagery.get_products(centroid, start_date, end_date)
        return er2_api.respond({"results": results})


class SceneState(ResetStateBase):
    def POST(self, *args, **kwargs):
        body = json.loads(self._request.body.decode("utf-8"))
        scene = body["scene"]
        use_satsearch = body.get("use_satsearch", False)

        # Save to state
        ret = self.get_scene(scene, use_satsearch)
        return er2_api.respond({"results": ret})


class InputState(ResetStateBase):
    """ReSET Input State"""

    def __init__(self, request=None, token=None):
        super().__init__(request=request, token=token)
        self.kind = "InputState"

    def download_climate(self):
        l = self._mapsource.get_layer("climate.shp")
        return l.download()

    def extract_climate(self, aoi, metadata, proj4):
        climate_path = os.path.join(self._mapsource.path, "climate.shp")
        if os.path.exists(climate_path):
            l = self._mapsource.get_layer("climate.shp")
            if l:
                self._mapsource.delete_layer(l)
            else:
                l = lf.layer_factory(climate_path)
                l.delete()

        extraction_date = datetime.datetime.strptime(
            "{0} {1}".format(
                metadata["DATE_ACQUIRED"], metadata["SCENE_CENTER_TIME"][1:6]
            ),
            "%Y-%m-%d %H:%M",
        )
        utc = pytz.utc
        extraction_date = utc.localize(extraction_date)

        climate = Climate(aoi)
        station_features = climate.extract_climate(extraction_date)
        gdf = geopandas.GeoDataFrame.from_features(station_features)
        gdf.geometry = gdf.geometry.apply(shape)
        gdf.crs = "epsg:4326"
        gdf.to_file(climate_path)
        scene_climate_df = gdf.to_crs(proj4)
        scene_climate_df.to_file(os.path.join(self._mapsource.path, "ws.shp"))

        if os.path.exists(climate_path):
            layer = self._mapsource.add_layer(climate_path)
            default_class = layer.view_list["default"]
            for sym in default_class.classifications:
                sym.size = 16
            default_class.labelsVisible = True
            default_class.labelField = "name"
            default_class.labelOffset = "2 2"
            default_class.labelPosition = "ur"

            layer.set_properties({"visible": True})
            layer.save_meta()
            self._mapSvc.update_and_write_mapfile()
        return {"results": scene_climate_df, "log": ["Climate extraction finished"]}

    def extract_dem(self, aoi, proj4, log, cell_size=None):
        start_time = time.time()
        workdir = self._mapsource.path
        dem_path = os.path.join(workdir, "dem.tif")
        if os.path.exists(dem_path):
            dem_layer = self._mapsource.get_layer_by_filepath(dem_path)
            self._mapsource.delete_layer(dem_layer)

        # For now assume single feature
        g = shape(aoi["features"][0]["geometry"])
        feat_wkt_4326 = dumps(g)

        url = "http://csip.engr.colostate.edu:8088/csip-watershed/m/extract_DEM/1.0"
        c = csip.utils.Client(url=url)
        c.add_data("bound_wkt", feat_wkt_4326)
        resp = c.execute()
        resp.download_data_files(["DEM_clip.tif"], workdir)
        # rename DEM_clip.tif to dem.tif
        os.rename(os.path.join(workdir, "DEM_clip.tif"), dem_path)
        dem_layer = lf.layer_factory(dem_path)
        dem_layer.reproject(proj4, cell_size=cell_size)
        dem_layer.load_raster()
        dem_layer.classify(classify_type="Equal Distance", nclass=5)
        dem_layer.set_properties({"visible": False, "is_reset": True})
        self._mapsource.add_layer(dem_layer)
        self._mapSvc.update_and_write_mapfile()
        log.append(f"DEM extraction finished in {time.time() - start_time}")
        return dem_layer

    @staticmethod
    def save_file(url, path):
        resp = requests.get(url)
        if resp.status_code == 200:
            with open(path, "wb") as f:
                for chunk in resp:
                    f.write(chunk)
        return path

    def cleanup(self):
        deleted = []
        for layer in self._mapsource.layers:
            if hasattr(layer, "is_reset"):
                layer.delete()
                deleted.append(layer.name)
        self._mapSvc.update_and_write_mapfile()
        return deleted

    def POST(self):
        body = json.loads(self._request.body.decode("utf-8"))
        scene_id, aoi = body["sceneID"], body["aoi"]

        imagery = Imagery()
        metadata = imagery.get_metadata(scene_id)
        bqa = imagery.get_band(scene_id, "BQA")
        proj4 = bqa["proj4"]

        log = []
        climate_results = self.extract_climate(aoi, metadata, proj4)
        climate_gp = climate_results["results"]
        features = []
        if climate_gp is not None:
            climate_geojson = climate_gp.__geo_interface__
            features = climate_geojson["features"]

            cell_size = [abs(bqa["x_res"]), abs(bqa["y_res"])]
            dem_layer = self.extract_dem(aoi, proj4, log, cell_size=cell_size)
        log += climate_results["log"]

        self.state = {
            "log": log,
            "climate": features,
            "reset_climate": self.get_reset_climate(),
        }
        return er2_api.respond(self)

    def DELETE(self, *args, **kwargs):
        """Remove any reset inputs and outputs"""
        self.deleted = self.cleanup()
        return er2_api.respond(self)


class ClimateState(ResetStateBase):
    """ReSET Input State"""

    def __init__(self, request=None, token=None):
        super().__init__(request=request, token=token)
        self._map_svc = svcs_map.MapState(request=self._request, token=token)
        self._mapsource = self._map_svc.get_mapsource("default")
        self.kind = "ClimateState"
        self.log = ""

    def GET(self):
        self.state = self.get_reset_climate()
        return er2_api.respond(self)

    def POST(self):
        upload_dir = tempfile.mkdtemp()
        climate_type = self._request.POST["climate_type"]
        shape_type = self._request.POST["shape_type"]

        # For now assume that POST means that layers are being added.
        if self._request.method == "POST":
            self._map_svc.write_request_files(self._request.FILES, upload_dir)
            self._map_svc.extract_archives_in_dir(upload_dir)
            layers = self._map_svc.add_layers_from_dir(upload_dir)
            for layer in layers:
                logging.getLogger("er2").info("Added {0}".format(layer))
                climate_fname = "user_{0}_{1}.shp".format(climate_type, shape_type)
                climate_pathname = os.path.join(self._mapsource.path, climate_fname)
                if os.path.exists(climate_pathname):
                    l = self._mapsource.get_layer_by_filepath(climate_pathname)
                    self._mapsource.delete_layer(l)
                user_layer = layer.copy_to(climate_pathname)

                # Reproject to 4326
                user_layer.reproject({"init": "EPSG:4326"})
                user_layer.set_properties({"visible": False})
                user_layer.save_meta()
                self._mapsource.add_layer(user_layer)

        self._map_svc.update_and_write_mapfile()
        shutil.rmtree(upload_dir)

        # Add new layers and update associated mapfile
        self.state = self.get_reset_climate()
        return er2_api.respond(self)

    def PATCH(self):
        """
        Update climate input layers
        :return:
        """
        body = json.loads(self._request.body.decode("utf-8"))
        shape_type = body["shape_type"]
        point_type = body["point_type"]
        feature_collection = body["feature_collection"]
        self.set_reset_climate(point_type, shape_type, feature_collection)
        return self.GET()


class ProcessSceneState(ResetStateBase):
    csip_host = os.getenv("CSIP_HOST", "csip") or "csip"
    process_url = "http://{0}:8080/csip-reset/m/process/1.0".format(csip_host)

    def __init__(self, request=None, token=None):
        super().__init__(request=request, token=token)
        self.kind = "ProcessSceneState"

    def gdal_grid(self, method, proj4, extent, input_path, output_path, **kwargs):
        """
        Interpolate using gdal_grid program.
        :param method: gdal_grid method type
        :param extent: [minx, miny, maxx, maxy]
        :param input_vector: layer.Vector
        :param output_path: destination path
        :param kwargs: gdal_grid arguments
        :return:
        """
        l, b, r, t = extent
        dx = abs(l - r)
        dy = abs(t - b)
        factor = 30
        size_x = dx / factor
        size_y = dy / factor
        if size_x > 1000 or size_y > 1000:
            calc_size_x = dx / 300
            calc_size_y = dy / 300
        else:
            calc_size_x = size_x
            calc_size_y = size_y

        layer_name = os.path.splitext(os.path.basename(input_path))[0]

        if method == "nearest":
            settings = {
                "model": "nearest",
                "radius1": kwargs.get("radius1", 0),
                "radius2": kwargs.get("radius2", 0),
                "angle": kwargs.get("angle", 0),
                "nodata": kwargs.get("nodata", 0),
            }
            model = "{model}:radius1={radius1}:radius2={radius2}:angle={angle}:nodata={nodata}".format(
                **settings
            )
        else:
            settings = {
                "model": "invdist",
                "power": kwargs.get("power", 2),
                "max_points": kwargs.get("max_points", 12),
                "smoothing": kwargs.get("smoothing", 2),
                "radius1": kwargs.get("radius1", 0),
                "radius2": kwargs.get("radius2", 0),
                "angle": kwargs.get("angle", 0),
                "nodata": kwargs.get("nodata", 0),
            }
            model = "{model}:power={power}:max_points={max_points}:smoothing={smoothing}:radius1={radius1}:radius2={radius2}:angle={angle}:nodata={nodata}".format(
                **settings
            )
        args = [
            "gdal_grid",
            "-zfield",
            kwargs["column"],
            "-l",
            layer_name,
            "-a",
            model,
            "-outsize",
            str(calc_size_x),
            str(calc_size_y),
            "-a_srs",
            proj4,
            "-txe",
            str(l),
            str(r),
            "-tye",
            str(b),
            str(t),
            input_path,
            output_path,
        ]
        output = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]
        logging.getLogger("er2").debug("gdal_grid args: {0}".format(" ".join(args)))
        logging.getLogger("er2").debug("Result of gdal_grid: {0}".format(output))

        # Resample to landsat resolution
        if calc_size_x < size_x or calc_size_y < size_y:
            h, tmpfile = tempfile.mkstemp(suffix=".tif")
            os.close(h)
            args = ["gdalwarp", "-tr", "30", "30", "-overwrite", output_path, tmpfile]
            output = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]
            logging.getLogger("er2").debug(
                "gdalwarp resample args: {0}".format(" ".join(args))
            )
            logging.getLogger("er2").debug(
                "Result of gdalwarp resample: {0}".format(output)
            )
            os.unlink(output_path)
            shutil.move(tmpfile, output_path)

        return {"raster": output_path, "settings": settings}

    def process_scene(self, body):
        """Prepare data for the ReSET model"""
        workdir = self._mapsource.path
        # Remove previous outputs
        layers_to_remove = []
        for l in self._mapsource.layers:
            if l.name[0] == "L" and "BQA" not in l.name:
                layers_to_remove.append(l)
        for l in layers_to_remove:
            self._mapsource.delete_layer(l)
        climate_layer = lf.layer_factory(os.path.join(workdir, "climate.shp"))
        climate_layer.set_properties({"visible": False, "is_reset": True})
        dem_layer = lf.layer_factory(os.path.join(workdir, "dem.tif"))
        dem_layer.set_properties({"visible": False, "is_reset": True})
        scene_id = body["sceneID"]
        aoi = body["aoi"]
        options = body["options"]

        # Remove ignored climate stations
        climate = body["climate"]
        stations = filter(
            lambda x: not x["properties"].get("ignore", False), climate["stations"]
        )
        stanames = list([s["properties"]["name"] for s in stations])
        climate_gp = geopandas.read_file(climate_layer.filepath)
        climate_gp = climate_gp.to_crs(dem_layer.proj4)
        climate_prime = climate_gp[climate_gp["name"].isin(stanames)]
        climate_path = os.path.join(os.path.dirname(climate_layer.filepath), "ws.shp")
        climate_prime.to_file(climate_path)

        aoi_shapefile_path = os.path.join(workdir, "aoi.shp")
        if os.path.exists(aoi_shapefile_path):
            l = self._mapsource.get_layer_by_filepath(aoi_shapefile_path)
            self._mapsource.delete_layer(l)

        # reproject AOI from 4326 to landsat projection.
        for i, f in enumerate(aoi["features"]):
            f["properties"] = {"name": "aoi", "OBJECTID": i + 1}
        g = geopandas.GeoDataFrame.from_features(aoi["features"])
        g.crs = {"init": "epsg:4326"}
        g.to_crs(dem_layer.proj4)
        if os.path.exists(aoi_shapefile_path):
            aoi_layer = self._mapsource.get_layer_by_filepath(aoi_shapefile_path)
            if aoi_layer:
                self._mapsource.delete_layer(aoi_layer)
            else:
                aoi_layer = lf.layer_factory(aoi_shapefile_path)
                aoi_layer.delete()

        g.to_file(aoi_shapefile_path)
        aoi_layer = lf.layer_factory(aoi_shapefile_path)
        aoi_layer.set_properties({"visible": False})
        self._mapsource.add_layer(aoi_layer)

        # First use IDW on climate layer
        for output_name, output_field in [
            ["climate_etr_hourly.tif", "etr_hourly_mm"],
            ["climate_etr.tif", "etr_mm"],
            ["climate_windrun_mile_day.tif", "wrun_mile_day"],
        ]:
            result_path = os.path.join(workdir, output_name)
            if os.path.exists(result_path):
                l = self._mapsource.get_layer_by_filepath(result_path)
                self._mapsource.delete_layer(l)

            # get the possibly shortened field name from the shapefile
            output_field_shp = None
            for k, v in climate_layer.schema["properties"].items():
                if k in output_field:
                    output_field_shp = k
                    break

            request_data = {
                "column": output_field_shp,
                "power": 2,
                "smoothing": 2,
                "max_points": 12,
                "radius1": 0,
                "radius2": 0,
                "angle": 0,
                "nodata": -999,
            }

            extent = [
                dem_layer.x_min,
                dem_layer.y_min,
                dem_layer.x_max,
                dem_layer.y_max,
            ]
            self.gdal_grid(
                "invdist",
                dem_layer.proj4,
                extent,
                climate_path,
                result_path,
                **request_data,
            )
            new_raster = lf.layer_factory(result_path)
            new_raster.classify(
                classify_type=er2_view.CLASSIFY_NATURAL_BREAKS, nclass=5
            )
            new_raster.set_properties({"visible": False, "is_reset": True})
            self._mapsource.add_layer(new_raster)
        self._mapSvc.update_and_write_mapfile()

        # Build CSIP payload
        # url = "http://129.82.52.49:8080/csip-reset/m/process/1.0"
        # url = "http://10.0.0.194:8080/csip-reset/m/process/1.0"
        c = csip.utils.Client(url=self.process_url)
        c.set_metainfo("keep_workspace", "true")
        c.set_metainfo("keep_results", 600000)
        c.add_data("scene_id", scene_id)
        c.add_data("etr_hourly", "climate_etr_hourly.tif")
        c.add_data("etr", "climate_etr.tif")
        c.add_data("wrun", "climate_windrun_mile_day.tif")
        c.add_data("dem", "dem.tif")
        c.add_data("aoi", "aoi.shp")
        c.add_data("weather stations", "ws.shp")
        for opt, value in options.items():
            c.add_data(opt, "1" if value["value"] else "0")

        has_hot_and_cold_points = body.get("hasHotAndColdPoints", False)
        has_hot_and_cold_aois = body.get("hasHotAndColdAOIs", False)
        if has_hot_and_cold_points:
            c.add_data("USER_COLD_POINTS_FILE", "cold_pts.shp")
            c.add_data("USER_HOT_POINTS_FILE", "hot_pts.shp")
        if has_hot_and_cold_aois:
            c.add_data("USER_COLD_AOI_FILE", "cold_aoi.shp")
            c.add_data("USER_HOT_AOI_FILE", "hot_aoi.shp")

        files = [
            os.path.join(workdir, "aoi.shp"),
            os.path.join(workdir, "aoi.dbf"),
            os.path.join(workdir, "aoi.prj"),
            os.path.join(workdir, "aoi.shx"),
            os.path.join(workdir, "climate_etr_hourly.tif"),
            os.path.join(workdir, "climate_etr.tif"),
            os.path.join(workdir, "climate_windrun_mile_day.tif"),
            os.path.join(workdir, "dem.tif"),
            os.path.join(workdir, "ws.shp"),
            os.path.join(workdir, "ws.dbf"),
            os.path.join(workdir, "ws.prj"),
            os.path.join(workdir, "ws.shx"),
        ]

        build_dir = os.path.join(workdir, "build")
        if os.path.exists(build_dir):
            shutil.rmtree(build_dir)
        os.mkdir(build_dir)

        for shape_type in ["pts", "aoi"]:
            if (shape_type == "pts" and has_hot_and_cold_points) or (
                shape_type == "aoi" and has_hot_and_cold_aois
            ):
                for point_type in ["hot", "cold"]:
                    point_shape = "{0}_{1}".format(point_type, shape_type)
                    path_basename = point_shape + ".shp"
                    path = os.path.join(workdir, "user_" + path_basename)
                    c.add_data(point_shape, path_basename)
                    if os.path.exists(path):
                        # Copy to build dir
                        l = lf.layer_factory(path)
                        new_l = l.copy_to(os.path.join(build_dir, path_basename))
                        new_l.reproject(dem_layer.proj4)
                    else:
                        # Just use AOI
                        l = lf.layer_factory(os.path.join(workdir, "aoi.shp"))
                        l.copy_to(os.path.join(build_dir, path_basename))

        for f in glob.glob(os.path.join(build_dir, "*")):
            files.append(f)

        # Add config file for gdal version
        config_path = os.path.join(build_dir, "AgroET_input.txt")
        with open(config_path, "w") as fp:
            fp.write("DATA_DIRECTORY=.\n")
            fp.write("LS_IMAGE_DIR=image\n")
            fp.write("DEM_FILE=dem.tif\n")
            fp.write("climate_windrun_mile_day=climate_windrun_mile_day.tif\n")
            fp.write("climate_etr_hourly=climate_etr_hourly.tif\n")
            fp.write("climate_etr=climate_etr.tif\n")
            fp.write("WS_FILE=ws.shp\n")
            fp.write("AOI_FILE=aoi.shp\n")
            if has_hot_and_cold_points:
                fp.write("USER_COLD_POINTS_FILE=cold_pts.shp\n")
                fp.write("USER_HOT_POINTS_FILE=hot_pts.shp\n")
            if has_hot_and_cold_aois:
                fp.write("USER_COLD_AOI_FILE=cold_aoi.shp\n")
                fp.write("USER_HOT_AOI_FILE=hot_aoi.shp\n")

        files.append(config_path)

        resp = c.execute(self.process_url, files=files, sync=False)

        if resp.get_error():
            raise Exception(resp.get_error())
        ret = resp.get_metainfo("suid")
        return ret

    def GET(self):
        """Currently this is a check for run status"""

        def get_scene_path(f, sceneID):
            return os.path.join(self._mapsource.path, sceneID + "_" + f)

        suid = self._request.GET["suid"]
        sceneID = self._request.GET["sceneID"]
        c = csip.utils.Client(url=self.process_url)
        c.set_metainfo(c.META_KEY_MODE, c.META_VAL_ASYNC)
        r = c.query_execute(suid)
        progress = r.get_metainfo("progress") or "No log yet..."
        progress = progress.split("\n")
        if r.get_status() == csip.utils.Client.STATUS_RUNNING:
            ret = {"suid": suid, "progress": progress}
        elif r.get_error():
            # if no layers, then an error must have occured. Return last line of log.
            log = r.get_error().split("\n")
            error = log[-1]
            ret = {
                "progress": progress + [r.get_error()],
                "error": error,
                "layers": [],
                "log": log,
            }
        else:
            # Remove previous results
            for f in r.get_data_files():
                base, ext = os.path.splitext(f)
                gis_data_path = get_scene_path(f, sceneID)
                if ext in lf.raster_exts and os.path.exists(gis_data_path):
                    layer = self._mapsource.get_layer_by_filepath(gis_data_path)
                    self._mapsource.delete_layer(layer)

            # r.download_data_files(dir=self._mapsource.path, files=r.get_data_files())
            for name in r.get_data_files():
                start_time = time.time()
                val = r.get_data_value(name)
                logging.getLogger("er2").info("  --> " + name)
                resp = requests.get(val, allow_redirects=True, stream=True)
                pathname = os.path.join(self._mapsource.path, name)
                with open(pathname, "wb") as f:
                    for chunk in resp.iter_content(chunk_size=8192):
                        if chunk:  # filter out keep-alive new chunks
                            f.write(chunk)
                logging.getLogger("er2").info(
                    f"{val} downloaded in {time.time() - start_time}"
                )

            log = "<no log>"
            layers = []
            for f in r.get_data_files():
                base, ext = os.path.splitext(f)
                ext = ext.lower()
                gis_data_path = os.path.join(self._mapsource.path, f)
                if ext in lf.raster_exts or ext in lf.vector_exts:
                    layer = lf.layer_factory(gis_data_path)
                    if ext in lf.raster_exts:
                        if "Composite" in layer.name:
                            layer.set_properties({"bands": [4, 2, 1]})
                        else:
                            layer.classify(er2_view.CLASSIFY_EQUAL_DISTANCE, nclass=6)
                            for sym in layer.view_list["default"].classifications:
                                sym.size = 20
                    layer.set_properties(
                        {"visible": False, "is_reset": True, "tags": [sceneID]}
                    )
                    layer.save_meta()
                    layers.append(layer)
                    self._mapsource.add_layer(layer)

            # Include climate layer.
            layer = lf.layer_factory(os.path.join(self._mapsource.path, "climate.shp"))
            layers.append(layer)

            self._mapSvc.update_and_write_mapfile()
            # Update state with mapsource with new layers
            self.set_state(self.state)
            ret = {"layers": layers, "progress": progress}

        self.state = ret
        return er2_api.respond(self)

    def POST(self):
        body = json.loads(self._request.body.decode("utf-8"))

        # TODO: fix args
        suid = self.process_scene(body)
        self.state = {"suid": suid}
        return er2_api.respond(self)
